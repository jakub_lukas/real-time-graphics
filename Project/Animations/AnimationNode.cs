﻿using System;
using OpenTK;

namespace Project.Animations
{
	class AnimationNode
	{
		public Vector3 Position = new Vector3(0f);
		public Quaternion Rotation = Quaternion.Identity;
		public Vector3 Scale = new Vector3(1f);

		public static AnimationNode Default { get { return new AnimationNode(); } }

		public static AnimationNode CreateAnimationNode(float positionX, float positionY, float positionZ, float eulerX, float eulerY, float eulerZ, float scaleX, float scaleY, float scaleZ)
		{
			return CreateAnimationNode(
				new Vector3(positionX, positionY, positionZ),
				new Vector3(eulerX, eulerY, eulerZ),
				new Vector3(scaleX, scaleY, scaleZ));
		}

		public static AnimationNode CreateAnimationNode(Vector3 position, Vector3 eulerAngles, Vector3 scale)
		{
			
			//var rotation = new Quaternion((float)(Math.PI * eulerAngles.Y / 180), (float)(Math.PI * eulerAngles.Z / 180), (float)(Math.PI * eulerAngles.X / 180), 1f);
			Quaternion rotateX = Quaternion.FromAxisAngle(Vector3.UnitX, MathHelper.DegreesToRadians(eulerAngles.X));
			Quaternion rotateY = Quaternion.FromAxisAngle(Vector3.UnitY, MathHelper.DegreesToRadians(eulerAngles.Y));
			Quaternion rotateZ = Quaternion.FromAxisAngle(Vector3.UnitZ, MathHelper.DegreesToRadians(eulerAngles.Z));
			Quaternion.Multiply(ref rotateZ, ref rotateY, out rotateY);
			Quaternion.Multiply(ref rotateX, ref rotateY, out rotateY);

			return new AnimationNode
			{
				Position = position,
				Rotation = rotateY,
				Scale = scale
			};
		}

		public static AnimationNode CreateLookAtPose(Vector3 from, Vector3 to, Vector3 up)
		{
			var result = new AnimationNode
			{
				Position = new Vector3(from)
			};

			// vyrob vektory predstavujuce suradnice bazickych vektorov rotacie vo svetovom systeme
			Vector3 direction = from - to;
			if (direction.Length <= .0001f)
				return result;

			direction.Normalize();
			up.Normalize();

			// vektorovy sucin pre direction a up vektory dava right vektor
			var right = Vector3.Cross(up, direction);
			right.Normalize();

			// vektorovy sucin pre direction a right vektory dava right vektor
			var newUp = Vector3.Cross(direction, right);
			newUp.Normalize();

			// tieto 3 vektory tvoria stlpce rotacnej matice
			var rotationMatrix = new Matrix3(right.X, newUp.X, direction.X,
											 right.Y, newUp.Y, direction.Y,
											 right.Z, newUp.Z, direction.Z);

			// vyrob kvaternion z rotacnej matice
			result.Rotation = Quaternion.FromMatrix(rotationMatrix);
			//result.Rotation.Normalize();

			return result;
		}

		public Matrix4 GetMatrix()
		{
			var translationMatrix = Matrix4.CreateTranslation(Position);
			var scaleMatrix = Matrix4.CreateScale(Scale);
			var rotationMatrix = Matrix4.CreateFromQuaternion(Rotation);

			return (scaleMatrix * rotationMatrix * translationMatrix);
		}

		#region Equals

		public override bool Equals(Object obj)
		{
			// If parameter is null return false.
			if (obj == null)
				return false;

			// If parameter cannot be cast to Point return false.
			var p = obj as AnimationNode;
			return Equals(p);
		}

		public bool Equals(AnimationNode p)
		{
			// If parameter is null return false:
			if ((object)p == null)
				return false;

			// Return true if the fields match:
			return (Position == p.Position) && (Rotation == p.Rotation) && (Scale == p.Scale);
		}

		public override int GetHashCode()
		{
			return Position.GetHashCode() ^ Rotation.GetHashCode() ^ Scale.GetHashCode();
		}

		public static bool operator ==(AnimationNode a, AnimationNode b)
		{
			// If both are null, or both are same instance, return true.
			if (System.Object.ReferenceEquals(a, b))
				return true;

			// If one is null, but not both, return false.
			if (((object)a == null) || ((object)b == null))
				return false;

			// Return true if the fields match:
			return a.Equals(b);
		}

		public static bool operator !=(AnimationNode a, AnimationNode b)
		{
			return !(a == b);
		}

		#endregion
	}
}
