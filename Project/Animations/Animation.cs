﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK;
using Project.Extensions;
using Project.Helpers;
using Quaternion = OpenTK.Quaternion;

namespace Project.Animations
{
	enum InterpolationType { Linear, Cubic }

	class Animation
	{
		#region properties
		public InterpolationType InterpolType = InterpolationType.Linear;
		public bool Loop = true;
		public bool IsPlaying { get; private set; }
		private float _timeElapsed;

		private readonly SortedDictionary<float, AnimationNode> _nodes = new SortedDictionary<float, AnimationNode>();
		private float _lastKeyTime;

		#endregion

		#region constructors and creators

		public static Animation CreateAnimation(List<float> knots, List<AnimationNode> values, bool loop = true)
		{
			if (knots.Count != values.Count) throw new Exception("Knots and Values have different lengths");

			var anim = new Animation { Loop = loop, IsPlaying = true };

			for (int i = 0; i < knots.Count; i++)
			{
				anim.AddAnimationNode(knots[i], values[i]);
			}

			return anim;
		}

		#endregion

		public void AddAnimationNode(float time, AnimationNode node)
		{
			_nodes.Add(time, node);

			_lastKeyTime = _nodes.Last().Key;
		}

		public void Update()
		{
			if (!IsPlaying) return;

			_timeElapsed += FpsCounter.DeltaTime;
			if (_timeElapsed <= _lastKeyTime) return;

			if (Loop)
				_timeElapsed %= _lastKeyTime;
			else
				Stop();
		}

		public Matrix4 GetMatrix()
		{
			return GetNode().GetMatrix();
		}

		public Vector3 GetPosition()
		{
			return GetNode().Position;
		}

		public Quaternion GetRotation()
		{
			return GetNode().Rotation;
		}

		public Vector3 GetScale()
		{
			return GetNode().Scale;
		}

		private AnimationNode GetNode()
		{
			if (_nodes.Count == 0)
				return AnimationNode.Default;

			if (InterpolType == InterpolationType.Linear)
			{
				return GetLinearInterpolatedNode();
			}

			return GetCubicInterpolatedNode();
		}

		#region Interpolations

		private AnimationNode GetLinearInterpolatedNode()
		{
			if (_nodes.Count == 1)
				return _nodes.First().Value;

			KeyValuePair<float, AnimationNode> n1;
			KeyValuePair<float, AnimationNode> n2;
			GetNodesForLinearInterpolation(out n1, out n2);

			float t = (_timeElapsed - n1.Key) / (n2.Key - n1.Key);

			return new AnimationNode
			{
				Position = (1 - t) * n1.Value.Position + t * n2.Value.Position,
				Rotation = Quaternion.Slerp(n1.Value.Rotation, n2.Value.Rotation, t),
				Scale = (1 - t) * n1.Value.Scale + t * n2.Value.Scale
			};
		}

		private void GetNodesForLinearInterpolation(out KeyValuePair<float, AnimationNode> node1, out KeyValuePair<float, AnimationNode> node2)
		{
			node1 = _nodes.First();

			foreach (var node in _nodes)
			{
				if (node.Key > _timeElapsed)
				{
					node2 = node;
					return;
				}
				node1 = node;
			}

			node2 = node1;
		}

		private AnimationNode GetCubicInterpolatedNode()
		{
			if (_nodes.Count == 1)
				return _nodes.First().Value;

			KeyValuePair<float, AnimationNode> n1;
			KeyValuePair<float, AnimationNode> n2;
			KeyValuePair<float, AnimationNode> n3;
			KeyValuePair<float, AnimationNode> n4;
			GetNodesForCubicInterpolation(out n1, out n2, out n3, out n4);

			var result = new AnimationNode();

			float t = (_timeElapsed - n2.Key) / (n3.Key - n2.Key);
			float b1 = 2*t*t*t - 3*t*t + 1;
			float b2 = t*t*t - 2*t*t + t;
			float b3 = -2*t*t*t + 3*t*t;
			float b4 = t*t*t - t*t;

			{
				Vector3 tangBegin = (n3.Key == n1.Key) ? Vector3.Zero : (n3.Value.Position - n1.Value.Position)/(n3.Key - n1.Key);
				Vector3 tangEnd = (n4.Key == n2.Key) ? Vector3.Zero : (n4.Value.Position - n2.Value.Position)/(n4.Key - n2.Key);

				result.Position = b1 * n2.Value.Position +
							b2 * Vector3.Multiply((n3.Value.Position - n2.Value.Position), tangBegin) +
							b3 * n3.Value.Position +
							b4 * Vector3.Multiply((n3.Value.Position - n2.Value.Position), tangEnd);
			}

			{
				// vypocitaj dalsie 2 pomocne bezierove vrcholy
				Quaternion Lj = QuaternionExt.DoubleArc(n1.Value.Rotation, n2.Value.Rotation);
				Quaternion Ljplus1 = QuaternionExt.DoubleArc(n2.Value.Rotation, n3.Value.Rotation);
				Quaternion Aj = QuaternionExt.BisectArc(Lj, n3.Value.Rotation);
				Quaternion Ajplus1 = QuaternionExt.BisectArc(Ljplus1, n4.Value.Rotation);
				Quaternion Bjplus1 = QuaternionExt.DoubleArc(Ajplus1, n3.Value.Rotation);

				// posun Bezierove vrcholy Aj a Bjplus1 blizsie k riadiacim vrcholom n2 a n3
				// teneto parameter definuje kvalitu interpolacnej krivky
				const float tension = 0.6666f;
				Aj.X = tension * n2.Value.Rotation.X + (1 - tension) * Aj.X;
				Aj.Y = tension * n2.Value.Rotation.Y + (1 - tension) * Aj.Y;
				Aj.Z = tension * n2.Value.Rotation.Z + (1 - tension) * Aj.Z;
				Aj.W = tension * n2.Value.Rotation.W + (1 - tension) * Aj.W;
				Aj.Normalize();
				Bjplus1.X = tension * n3.Value.Rotation.X + (1 - tension) * Bjplus1.X;
				Bjplus1.Y = tension * n3.Value.Rotation.Y + (1 - tension) * Bjplus1.Y;
				Bjplus1.Z = tension * n3.Value.Rotation.Z + (1 - tension) * Bjplus1.Z;
				Bjplus1.W = tension * n3.Value.Rotation.W + (1 - tension) * Bjplus1.W;
				Bjplus1.Normalize();

				// mame bezierovy vrcholy na 4D sfere, pouzi deCasteljauov algoritmus a slerp
				Quaternion P1 = Quaternion.Slerp(n2.Value.Rotation, Aj, t);
				Quaternion P2 = Quaternion.Slerp(Aj, Bjplus1, t);
				Quaternion P3 = Quaternion.Slerp(Bjplus1, n3.Value.Rotation, t);
				Quaternion P4 = Quaternion.Slerp(P1, P2, t);
				Quaternion P5 = Quaternion.Slerp(P2, P3, t);
				Quaternion Q = Quaternion.Slerp(P4, P5, t);

				result.Rotation = Q;
			}

			{
				Vector3 tangBegin = (n3.Key == n1.Key) ? Vector3.Zero : (n3.Value.Scale - n1.Value.Scale) / (n3.Key - n1.Key);
				Vector3 tangEnd = (n4.Key == n2.Key) ? Vector3.Zero : (n4.Value.Scale - n2.Value.Scale) / (n4.Key - n2.Key);

				result.Position = b1 * n2.Value.Scale +
							b2 * Vector3.Multiply((n3.Value.Scale - n2.Value.Scale), tangBegin) +
							b3 * n3.Value.Scale +
							b4 * Vector3.Multiply((n3.Value.Scale - n2.Value.Scale), tangEnd);
			}

			return result;
		}

		private void GetNodesForCubicInterpolation(out KeyValuePair<float, AnimationNode> node1, out KeyValuePair<float, AnimationNode> node2, out KeyValuePair<float, AnimationNode> node3, out KeyValuePair<float, AnimationNode> node4)
		{
			int index = _nodes.TakeWhile(node => (node.Key <= _timeElapsed)).Count();
			int count = _nodes.Count;

			node2 = _nodes.ElementAt(index);
			if (Loop)
			{
				node1 = (index > 0) ? _nodes.ElementAt(index - 1) : _nodes.ElementAt(count - 1);
				node3 = (index < count - 1) ? _nodes.ElementAt(index + 1) : _nodes.ElementAt(0);
				node4 = (index < count - 2)
					? _nodes.ElementAt(index + 2)
					: (index < count - 1) ? _nodes.ElementAt(0) : _nodes.ElementAt(1);
			}
			else
			{
				node1 = (index > 0) ? _nodes.ElementAt(index - 1) : node2;
				node3 = (index < count - 1) ? _nodes.ElementAt(index + 1) : node2;
				node4 = (index < count - 2) ? _nodes.ElementAt(index + 2) : node3;
			}
		}

		#endregion

		#region Play controls

		public void Play()
		{
			IsPlaying = true;
		}

		public void Pause()
		{
			IsPlaying = false;
		}

		public void Stop()
		{
			IsPlaying = false;
			_timeElapsed = 0f;
		}

		#endregion
	}
}
