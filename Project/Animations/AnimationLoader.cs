﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using OpenTK;

namespace Project.Animations
{
	class AnimationLoader
	{
		public const string Directory = "animations";

		private static AnimationNode CreateNode(string type, Vector3 vector1, Vector3 vector2, Vector3 vector3)
		{
			switch (type)
			{
				case "pose":
					return AnimationNode.CreateAnimationNode(vector1, vector2, vector3);
				case "look":
					return AnimationNode.CreateLookAtPose(vector1, vector2, vector3);
			}
			return null;
		}

		public static Animation LoadAnimation(string filename)
		{
			using (var reader = new StreamReader(Directory + "/" + filename))
			{
				var knots = new List<float>();
				var values = new List<AnimationNode>();

				string line;
				while ((line = reader.ReadLine()) != null)
				{
					if (line.Length == 0) continue;

					string[] parts = line.Split(' ');
					if (parts.Length != 2) throw new Exception("bad file");
					string type = parts[0];
					float time = float.Parse(parts[1], CultureInfo.InvariantCulture);

					line = reader.ReadLine();
					if (line == null) break;
					parts = line.Split(' ');
					if (parts.Length != 3) throw new Exception("bad file");
					var vec1 = new Vector3(
						float.Parse(parts[0], CultureInfo.InvariantCulture),
						float.Parse(parts[1], CultureInfo.InvariantCulture),
						float.Parse(parts[2], CultureInfo.InvariantCulture));

					line = reader.ReadLine();
					if (line == null) break;
					parts = line.Split(' ');
					if (parts.Length != 3) throw new Exception("bad file");
					var vec2 = new Vector3(
						float.Parse(parts[0], CultureInfo.InvariantCulture),
						float.Parse(parts[1], CultureInfo.InvariantCulture),
						float.Parse(parts[2], CultureInfo.InvariantCulture));

					line = reader.ReadLine();
					if (line == null) break;
					parts = line.Split(' ');
					if (parts.Length != 3) throw new Exception("bad file");
					var vec3 = new Vector3(
						float.Parse(parts[0], CultureInfo.InvariantCulture),
						float.Parse(parts[1], CultureInfo.InvariantCulture),
						float.Parse(parts[2], CultureInfo.InvariantCulture));

					knots.Add(time);
					values.Add(CreateNode(type, vec1, vec2, vec3));
				}

				return Animation.CreateAnimation(knots, values);
			}

			return null;
		}
	}
}
