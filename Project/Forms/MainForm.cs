﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using Project.Animations;
using Project.Helpers;
using Project.Scene;
using Project.Scene.Cameras;
using Project.Scene.Helpers;
using Project.Scene.Lights;
using Project.Scene.Nodes;
using Project.Scene.ParticleSystem;
using Project.Scene.Primitives;

namespace Project.Forms
{
	public partial class MainForm : Form
	{
		#region Attributes

		private bool _showHelp = false;
		private bool _texturing = true;
		private bool _shadows = true;
		private bool _linearInterpolation = true;

		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		private Scene.Scene _world;
		private List<Scene.Scene> _models = new List<Scene.Scene>();

		private TextWriter _tw;

		#endregion

		void OnLoad()
		{
			Renderer.InitGL();
			Renderer.InitShaders();

			_world = new Scene.Scene();

			Scene.Scene r2d2 = SceneLoader.Load("r2d2/R2D2.dae");
			Animation animR2d2 = AnimationLoader.LoadAnimation("r2d2.txt");
			_world.Animations.Add(animR2d2);
			r2d2.Nodes[0].Animation = animR2d2;
			_models.Add(r2d2);

			//_world = SceneLoader.Load("Humvee/HMMWV.3ds");
			//_world = SceneLoader.Load("treasure_chest/treasure_chest.dae");

			Scene.Scene arc170 = SceneLoader.Load("arc_170/arc_170.dae");
			Animation animArc170 = AnimationLoader.LoadAnimation("arc_170.txt");
			_world.Animations.Add(animArc170);
			arc170.Nodes[0].ChildrenNodes[2].ChildrenNodes[0].Animation = animArc170;
			_models.Add(arc170);

			Scene.Scene interceptor = SceneLoader.Load("federation_interceptor/federation_interceptor_flying.dae");
			Animation animInterceptor = AnimationLoader.LoadAnimation("interceptor.txt");
			_world.Animations.Add(animInterceptor);
			interceptor.Nodes[0].Animation = animInterceptor;
			_models.Add(interceptor);

			// skybox
			Mesh skyboxMesh = Skybox.CreateMesh(500f);
			_world.Materials["skybox"] = Skybox.CreateMaterial();
			skyboxMesh.MaterialName = "skybox";
			_world.Skybox = skyboxMesh;

			Animation animCamera = AnimationLoader.LoadAnimation("camera.txt");
			Camera camera = new FpsCamera(new Vector3(-30, 15f, 30), -Vector3.UnitZ)//new OrbitCamera(10f, 0f, 90f)
			{
				Animation = animCamera//, MovementFocus = true
			};
			_world.Animations.Add(animCamera);
			_world.Cameras.Add("Main", camera);
			_world.MainCamera = camera;

			// SUN
			_world.Materials["sunMaterial"] = new Material { DiffuseTextureGLId = Texture.Load("textures/sun.jpg"), Shader = "noShading" };
			Mesh circleMesh = Sphere.CreateMesh(32, 32, 8f);
			circleMesh.MaterialName = "sunMaterial";
			var circleNode = new LightNode(new Vector3(-280, 280, 0), Vector3.Zero, Vector3.UnitY) { Mesh = circleMesh, Diffuse = new Color4(128, 128, 80, 255), Type = LightType.Directional };
			Animation animSun = AnimationLoader.LoadAnimation("sun.txt");
			circleNode.Animation = animSun;
			_world.Animations.Add(animSun);
			_world.Lights.Add(circleNode);

			// FEDERATION SHIP LIGHT
			_world.Materials["arc170Material"] = new Material { ColorDiffuse = new Color4(0, 255, 255, 255), Shader = "noShading" };
			Mesh circleMesh2 = Sphere.CreateMesh(32, 32, 0.1f);
			circleMesh2.MaterialName = "arc170Material";
			var federationLight = new LightNode(new Vector3(0, 4, 8), Vector3.Zero, Vector3.UnitY)
			{
				Mesh = circleMesh2, Diffuse = new Color4(0, 80, 80, 255), Type = LightType.Point,
				Specular = new Color4(0.1f, 0.1f, 0.1f, 1f)
			};
			_world.Lights.Add(federationLight);

			// FIGHTER LIGHT
			var arc170Light = new LightNode(new Vector3(0, 0, 0), Vector3.Zero, Vector3.UnitY)
			{
				Mesh = circleMesh2,
				Diffuse = new Color4(60, 0, 0, 255),
				Type = LightType.Point,
				Specular = new Color4(0f, 0f, 0f, 1f)
			};
			arc170Light.Animation = animArc170;
			_world.Lights.Add(arc170Light);

			//plane
			//_world.Materials["planeMaterial"] = new Material
			//{
				//DiffuseTextureGLId = Texture.Load("textures/cobblestone_diffuse_map.jpg"),
				//NormalTextureGLId = Texture.Load("textures/cobblestone_normal_map.jpg")
			//};
			Node planeNode = Plane.CreateNode(Vector3.Zero, new Vector3(0f, 200f, 0f));
			//planeNode.Mesh.MaterialName = "planeMaterial";
			_world.Nodes.Add(planeNode);

			_world.ParticleSystems.Add(Emitter.CreateEmitter(new Vector3(-5, 4, -13), new Vector3(0, 1, 0), "textures/fire.png", "textures/smoke.png", -0.02f));
			_world.ParticleSystems.Add(Emitter.CreateEmitter(new Vector3(0, 7.5f, -10), new Vector3(0, 4, 0), "textures/lightning.png", "textures/lightning.png", 0.5f));


			// HUD text
			_tw = new TextWriter(glControl.Size, new Size(glControl.Width / 2, glControl.Height / 2));
			_tw.AddLine("Projekt GRC - Base Core & Libraries ...", new PointF(0, 0), new SolidBrush(Color.Yellow));
			_tw.AddLine("F1 - zapni/vypni help", new PointF(0, 20), new SolidBrush(Color.Yellow));
			_tw.AddLine("F2 - play/pause, F10 - stop animacii", new PointF(0, 40), new SolidBrush(Color.Yellow));
			_tw.AddLine("F3 - zapni/vypni textury", new PointF(0, 60), new SolidBrush(Color.Yellow));
			//_tw.AddLine("F3 - zapni/vypni linearnu interpolaciu", new PointF(0, 60), new SolidBrush(Color.Yellow));
		}

		new void Update()
		{
			_world.Update();
		}

		void Render()
		{
			Renderer.RenderScene(_world, _models, _tw, _texturing, _shadows, _showHelp);
			glControl.SwapBuffers();
		}

		void OnClose()
		{

		}
	}
}
