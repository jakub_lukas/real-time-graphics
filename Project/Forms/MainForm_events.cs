﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Assimp;
using OpenTK.Graphics;
using Project.Helpers;
using Project.Inputs;
using Project.Scene.Helpers;

namespace Project.Forms
{
	public partial class MainForm
	{
		private bool _loaded;
		private FpsCounter _fpsCounter;

		private bool _mouseLocked;
		private Point _origCursorPosition;
		private Point _windowCenter;

		#region Form

		public MainForm()
		{
			InitializeComponent();
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			OnClose();
		}

		void Application_Idle(object sender, EventArgs e)
		{
			_fpsCounter.Update();
			UpdateMouseMovement();

			Update();

			Input.Instance.AfterUpdate();
			glControl.Invalidate();
		}

		#endregion

		#region glControl

		private void glControl_Load(object sender, EventArgs e)
		{
			_loaded = true;
			GraphicsContext.CurrentContext.SwapInterval = 1;
			Application.Idle += Application_Idle;
			_fpsCounter = new FpsCounter();
			_fpsCounter.Start();
			OnLoad();
		}

		private void glControl_Paint(object sender, PaintEventArgs e)
		{
			if (!_loaded) return;

			Render();
		}

		private void glControl_Resize(object sender, EventArgs e)
		{
			_windowCenter = glControl.PointToScreen(new Point(glControl.Width / 2, glControl.Height / 2));
			Renderer.Resize(glControl.Width, glControl.Height);
			glControl.Invalidate();
		}

		#endregion

		#region Mouse

		protected void ResetCursorPosition()
		{
			Cursor.Position = _windowCenter;
		}

		protected void LockMouse()
		{
			_mouseLocked = true;
			_origCursorPosition = Cursor.Position;
			Cursor.Hide();
			ResetCursorPosition();
		}

		protected void UnlockMouse()
		{
			_mouseLocked = false;
			Cursor.Show();
			Cursor.Position = _origCursorPosition;
		}

		protected void UpdateMouseMovement()
		{
			Point mouseDelta = Cursor.Position - new Size(_windowCenter);
			Input.Instance.SetMovement(mouseDelta.X, -mouseDelta.Y);
			if (_mouseLocked)
				ResetCursorPosition();
		}

		private void glControl_MouseDown(object sender, MouseEventArgs e)
		{
			Input.Instance.SetMouseDown(e.Button);
			LockMouse();
		}

		private void glControl_MouseUp(object sender, MouseEventArgs e)
		{
			Input.Instance.SetMouseUp(e.Button);
			UnlockMouse();
		}

		private void Form1_MouseWheel(object sender, MouseEventArgs e)
		{
			Input.Instance.SetMouseWheel(e.Delta);
		}

		#endregion

		#region Key

		private void glControl_KeyDown(object sender, KeyEventArgs e)
		{
			Input.Instance.SetKeyDown(e.KeyCode);

			switch (e.KeyCode)
			{
				case Keys.Escape:
					Close();
					break;

				case Keys.F1:
					_showHelp = !_showHelp;
					break;

				case Keys.F2:
					for (int i = 0; i < _world.Animations.Count; ++i)
					{
						if (_world.Animations[i].IsPlaying)
							_world.Animations[i].Pause();
						else
							_world.Animations[i].Play();
					}
					for (int i = 0; i < _world.ParticleSystems.Count; ++i)
					{
						if (_world.ParticleSystems[i].IsPlaying)
							_world.ParticleSystems[i].Pause();
						else
							_world.ParticleSystems[i].Play();
					}
					break;

				case Keys.F3:
					_texturing = !_texturing;
					break;

				case Keys.F4:
					_shadows = !_shadows;
					break;

				case Keys.F5:
					_linearInterpolation = !_linearInterpolation;
					break;

				case Keys.F10:
					_world.Animations.ForEach(a => a.Stop());
					break;
			}
		}

		private void glControl_KeyUp(object sender, KeyEventArgs e)
		{
			Input.Instance.SetKeyUp(e.KeyCode);
		}

		#endregion
	}
}
