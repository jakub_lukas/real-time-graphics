#version 150 compatibility

#ifdef GL_ES
	precision mediump float;
	precision mediump int;
	precision lowp sampler2D;
	precision lowp samplerCube;
#endif

struct Material {
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 emission;
	float shininess;
	//int texCount;
};

struct LightSource {
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	vec4 halfVector;
	vec3 spotDirection;
	float spotExponent;
	float spotCutoff;
	float spotCosCutoff;
	float constantAttenuation;
	float linearAttenuation;
	float quadraticAttenuation;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	int hasShadowMap;
};

//////////////////////////////////////////////////////////////

#define MAX_LIGHTS 4

uniform int texturing_enabled;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;


uniform sampler2D diffuse_map;
uniform sampler2D normal_map;
uniform sampler2D shadowMaps[MAX_LIGHTS];


uniform Material material;
uniform LightSource lightSource[MAX_LIGHTS];


varying vec2 v_texcoord;
varying vec4 v_shadowMapTexCoords[MAX_LIGHTS];

varying vec3 v;
varying vec3 n;
varying vec3 p;
varying vec4 T_eye;
varying vec4 B_eye;

float inShadow(in int i)
{
	if (i == 0 && lightSource[0].hasShadowMap > 0)
	{
		return (texture(shadowMaps[0], v_shadowMapTexCoords[0].xy).r * 1.02 > v_shadowMapTexCoords[0].z) ? 1.0 : 0.0;
	}
	else if (i == 1 && lightSource[1].hasShadowMap > 0)
	{
		return (texture(shadowMaps[1], v_shadowMapTexCoords[1].xy).r * 1.02 > v_shadowMapTexCoords[1].z) ? 1.0 : 0.0;
	}
	else if (i == 2 && lightSource[2].hasShadowMap > 0)
	{
		return (texture(shadowMaps[2], v_shadowMapTexCoords[2].xy).r * 1.02 > v_shadowMapTexCoords[2].z) ? 1.0 : 0.0;
	}
	else if (i == 3 && lightSource[3].hasShadowMap > 0)
	{
		return (texture(shadowMaps[3], v_shadowMapTexCoords[3].xy).r * 1.02 > v_shadowMapTexCoords[3].z) ? 1.0 : 0.0;
	}

	return 1.0;
}

vec4 shadingModel_Blinn(in int i, in vec4 diffuseMaterial, in vec4 specularMaterial, in vec3 V, in vec3 N)
{
	vec3 L = normalize(viewMatrix * lightSource[i].position - viewMatrix * modelMatrix * vec4(p, 1.0)).xyz;
	vec3 H = normalize(V + L);

	float diffuse = max(dot(L, N), 0.0);
	float specular = pow(clamp(dot(N, H), 0.0, 1.0), material.shininess);

	float shd = inShadow(i);
	diffuse = shd * diffuse;
	specular = specular * floor(shd);

	return (diffuseMaterial * diffuse * lightSource[i].diffuse) + (specularMaterial * specular * lightSource[i].specular);
}


void main(void)
{
	vec4 diffuseMaterial = material.diffuse * ((1 - texturing_enabled) + texturing_enabled * texture(diffuse_map, v_texcoord));

	vec3 V = normalize(v);
	vec3 N = normalize(n);

	vec4 normal_eye = vec4(N, 0.0);
	vec4 T = normalize(T_eye);
	//T = T - dot(N, T) * N;
	vec4 B = normalize(B_eye);

	vec3 normal = 2 * texture2D(normal_map, v_texcoord).rgb - 1;
	normal_eye.x = T.x * normal.x + B.x * normal.y + N.x * normal.z;
	normal_eye.y = T.y * normal.x + B.y * normal.y + N.y * normal.z;
	normal_eye.z = T.z * normal.x + B.z * normal.y + N.z * normal.z;
	normal_eye.w = 0;
	normal_eye = normalize(normal_eye);

	vec4 resColor = material.ambient * lightSource[0].ambient;
	for (int i = 0; i < MAX_LIGHTS; ++i)
	{
		resColor += shadingModel_Blinn(i, diffuseMaterial, material.specular, V, normal_eye.xyz);
	}

	gl_FragColor = resColor;
}
