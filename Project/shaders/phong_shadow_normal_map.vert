#version 150 compatibility

#ifdef GL_ES
	precision mediump float;
	precision mediump int;
	precision lowp sampler2D;
	precision lowp samplerCube;
#endif

struct Material {
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 emission;
	float shininess;
	//int texCount;
};

struct LightSource {
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	vec4 halfVector;
	vec3 spotDirection;
	float spotExponent;
	float spotCutoff;
	float spotCosCutoff;
	float constantAttenuation;
	float linearAttenuation;
	float quadraticAttenuation;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	int hasShadowMap;
};

//////////////////////////////////////////////////////////////

#define MAX_LIGHTS 4

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform LightSource lightSource[MAX_LIGHTS];


attribute vec3 a_vertex;
attribute vec2 a_texcoord;
attribute vec3 a_normal;
attribute vec3 a_tangent;
attribute vec3 a_bitangent;

varying vec2 v_texcoord;
varying vec4 v_shadowMapTexCoords[MAX_LIGHTS];

varying vec3 v;
varying vec3 n;
varying vec3 p;
varying vec4 T_eye;
varying vec4 B_eye;

void main(void)
{
	v_texcoord = a_texcoord;
	T_eye = transpose(inverse(viewMatrix * modelMatrix)) * vec4(a_tangent, 1.0);
	B_eye = transpose(inverse(viewMatrix * modelMatrix)) * vec4(a_bitangent, 1.0);

	vec4 vertex = vec4(a_vertex, 1.0);
	mat4 modelView = viewMatrix * modelMatrix;

	v = -vec3(modelView * vertex);
	n = vec3(transpose(inverse(modelView)) * vec4(a_normal, 1.0));
	p = a_vertex;
	
	for (int i = 0; i < MAX_LIGHTS; ++i)
	{
		v_shadowMapTexCoords[i] = lightSource[i].projectionMatrix * lightSource[i].viewMatrix * modelMatrix * vertex;
		v_shadowMapTexCoords[i] = 0.5 * v_shadowMapTexCoords[i] / v_shadowMapTexCoords[i].w + 0.5;
	}

	gl_Position = projectionMatrix * modelView * vertex;
}
