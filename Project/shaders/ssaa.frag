uniform sampler2D sampler;

void main(void)
{
  gl_FragColor = texture2D(sampler, gl_TexCoord[0].st);
}