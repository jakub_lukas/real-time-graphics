varying vec2 vTexCoord;

uniform sampler2D diffuse_map;
uniform sampler2D ao_map;
uniform int texturing_enabled;

void main(void)
{
	vec4 diffuse_color = vec4(1, 1, 1, 1);
	if (texturing_enabled > 0)
		diffuse_color = texture2D(diffuse_map, vTexCoord);
   gl_FragColor = texture2D(ao_map, vTexCoord).r * diffuse_color;
}

