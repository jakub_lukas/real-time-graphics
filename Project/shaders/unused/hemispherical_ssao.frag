varying vec2 vTexCoord;
uniform sampler2D normal_depth_map;
uniform sampler2D random_map;
uniform sampler2D depth_map;
uniform mat4 projMatrix;
uniform mat4 invProjMatrix;
uniform float near, far, checkRadius;

const vec3 sampleKernel[8] = vec3[8](
    vec3(-0.011331562, -0.074309729, 0.033523623),
    vec3(-0.016038071, -0.073256440, 0),
    vec3(0.031890523, -0.050860822, 0.0052353325),
    vec3(0.016236259, 0.027160129, 0.0068024951),
    vec3(-0.30093661, -0.10254280, 0.0061832811),
    vec3(0.064812385, -0.19595113, 0.20066050),
    vec3(-0.032532435, -0.22817777, 0.057787783),
    vec3(0.31232032, 0.57163316, 0.023146300)
);

float unpack_depth(vec4 rgba_depth)
{
    const vec4 bit_shift = vec4(1.0/(256.0*256.0*256.0), 1.0/(256.0*256.0), 1.0/256.0, 1.0);
    float depth = dot(rgba_depth, bit_shift);
    return depth;
}

void main(void)
{
    vec4 normal_depth = texture2D(normal_depth_map, vTexCoord);
	float normalized_depth = unpack_depth(texture2D(depth_map, vTexCoord));
	
    // reconstruct eye-space position of fragment from linear depth
    float T1 = projMatrix[2][2];
    float T2 = projMatrix[2][3];
    float E1 = projMatrix[3][2];
    float linear_depth = -(near + (far - near) * normalized_depth);
    vec4 pos_clip;
    pos_clip.w = E1 * linear_depth;
    pos_clip.x = pos_clip.w * (2.0 * vTexCoord.x - 1.0);
	pos_clip.y = pos_clip.w * (2.0 * vTexCoord.y - 1.0);
    pos_clip.z = (T1 * linear_depth + T2);
    vec4 pos_eye = invProjMatrix * pos_clip;
    
    // get TBN matrix for transforming samples from TBN to eye-space
    vec3 normal = normal_depth.xyz * 2.0 - 1.0;
    normal = normalize(normal);
    vec3 randomVec = texture2D(random_map, 100 * vTexCoord).xyz * 2.0 - 1.0;
    vec3 tangent = normalize(randomVec - normal * dot(randomVec, normal));
    vec3 bitangent = cross(normal, tangent);
    mat3 TBN = mat3(tangent, bitangent, normal);

     float occlusion = 0.0;
     for (int i = 0; i < 8; i++) 
     {
        // get sample position in eye space
        vec3 sample_eye = TBN * sampleKernel[i];
        sample_eye = sample_eye * checkRadius + pos_eye.xyz;

        // project sample position to NDC 
        vec4 sample_ndc = projMatrix * vec4(sample_eye, 1.0);

        // get depth of sample from depth texture
        float sampleDepth = unpack_depth(texture2D(depth_map, 0.5 * (sample_ndc.xy / sample_ndc.w) + 0.5));
        sampleDepth = -(near + (far - near) * sampleDepth);

        // range check & accumulate
        float rangeCheck = abs(pos_eye.z - sampleDepth) < checkRadius ? 1.0 : 0.0;
        occlusion += (sampleDepth <= sample_eye.z ? 1.0 : 0.0) * rangeCheck;
    }
    gl_FragColor = vec4(occlusion / 8);
}

