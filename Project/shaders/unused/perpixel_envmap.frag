varying vec4 R_world;

uniform sampler2D diffuse_map;
uniform samplerCube cube_map;
uniform int texturing_enabled;

void main(void)
{
   vec3 R = normalize(R_world.yzx);
   vec4 color = textureCube(cube_map, R);

   if (texturing_enabled > 0)
       color = mix(color, texture2D(diffuse_map, gl_TexCoord[0].st), 0.5);

   gl_FragColor = color;
}
