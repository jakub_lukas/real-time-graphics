varying vec4 V_eye;
varying vec4 L_eye;
varying vec4 N_eye;

uniform sampler2D diffuse_map;
uniform int texturing_enabled;
uniform int lighting_enabled;

void main(void)
{
   vec4 diffuse_material = gl_FrontMaterial.diffuse;
   if (texturing_enabled > 0)
       diffuse_material = texture2D(diffuse_map, gl_TexCoord[0].st);
	   
	
   vec4 N = vec4(1,0,0,1);
   if (lighting_enabled > 0)
		N = 0.5 * normalize(N_eye) + 0.5; 

   gl_FragData[0] = diffuse_material;
   gl_FragData[1] = vec4(N.xyz, 1);
}
