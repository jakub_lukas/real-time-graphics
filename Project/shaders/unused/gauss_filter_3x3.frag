varying vec2 vTexCoord;

uniform sampler2D map;
uniform int map_width, map_height;

void main() 
{
    vec4 sum = vec4(0.0);

    //our original texcoord for this fragment
    vec2 tc = vTexCoord;
    float stepx = 1.0 / map_width;
	float stepy = 1.0 / map_height;

    //apply blurring, using a 9-tap filter with predefined gaussian weights
    sum += texture2D(map, vec2(tc.x - 4.0*stepx, tc.y - 4.0*stepy)) * 0.0162162162;
    sum += texture2D(map, vec2(tc.x - 3.0*stepx, tc.y - 3.0*stepy)) * 0.0540540541;
    sum += texture2D(map, vec2(tc.x - 2.0*stepx, tc.y - 2.0*stepy)) * 0.1216216216;
    sum += texture2D(map, vec2(tc.x - 1.0*stepx, tc.y - 1.0*stepy)) * 0.1945945946;

    sum += texture2D(map, vec2(tc.x, tc.y)) * 0.2270270270;

    sum += texture2D(map, vec2(tc.x + 1.0*stepx, tc.y + 1.0*stepy)) * 0.1945945946;
    sum += texture2D(map, vec2(tc.x + 2.0*stepx, tc.y + 2.0*stepy)) * 0.1216216216;
    sum += texture2D(map, vec2(tc.x + 3.0*stepx, tc.y + 3.0*stepy)) * 0.0540540541;
    sum += texture2D(map, vec2(tc.x + 4.0*stepx, tc.y + 4.0*stepy)) * 0.0162162162;

    gl_FragColor = sum;
}

