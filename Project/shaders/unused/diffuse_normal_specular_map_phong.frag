varying vec4 V_eye;
varying vec4 L_eye;
varying vec4 N_eye;
varying vec4 T_eye;

uniform sampler2D diffuse_map;
uniform sampler2D normal_map;
uniform sampler2D specular_map;
uniform int texturing_enabled;
uniform int lighting_enabled;

void main(void)
{
   vec4 diffuse_material = gl_FrontMaterial.diffuse;
   if (texturing_enabled > 0)
       diffuse_material = texture2D(diffuse_map, gl_TexCoord[0].st);
   vec4 specular_material = gl_FrontMaterial.specular;
   if (texturing_enabled > 0)
       specular_material = texture2D(specular_map, gl_TexCoord[0].st);
	   
   vec4 color = diffuse_material;
   if (lighting_enabled > 0)
   {
	   vec4 V = normalize(V_eye);
   	   vec4 L = normalize(L_eye);
   	   vec4 N = normalize(N_eye);
       vec4 T = normalize(T_eye);
	   T = T - dot(N, T) * N;
       vec4 B = normalize(vec4(T.w * cross(N.xyz, T.xyz), 0));	   

	   vec3 normal = 2 * texture2D(normal_map, gl_TexCoord[0].st).rgb - 1;
	   vec4 normal_eye;
	   normal_eye.x = T.x * normal.x + B.x * normal.y + N.x * normal.z;
	   normal_eye.y = T.y * normal.x + B.y * normal.y + N.y * normal.z;
	   normal_eye.z = T.z * normal.x + B.z * normal.y + N.z * normal.z;
	   normal_eye.w = 0;
	   normal_eye = normalize(normal_eye);
	   
	   float diffuse = clamp(dot(L, normal_eye), 0.0, 1.0);
   	   vec4 R = reflect(-L, normal_eye);
   	   float specular = sign(diffuse) * pow(clamp(dot(R, V), 0.0, 1.0), gl_FrontMaterial.shininess);

	   color = 0.2 * (vec4(0.2, 0.2, 0.2, 1.0) + gl_LightSource[0].ambient) * (gl_FrontMaterial.ambient + diffuse_material);
	   color += diffuse * gl_LightSource[0].diffuse * diffuse_material;
	   color += specular * gl_LightSource[0].specular * specular_material;
   }

   gl_FragColor = color;
}
