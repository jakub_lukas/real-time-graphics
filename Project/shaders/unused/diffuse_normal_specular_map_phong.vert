attribute vec4 tangent;

varying vec4 V_eye;
varying vec4 L_eye;
varying vec4 N_eye;
varying vec4 T_eye;

void main(void)
{
   V_eye = gl_ModelViewMatrix * gl_Vertex;
   L_eye = gl_LightSource[0].position - V_eye;
   N_eye = vec4(gl_NormalMatrix * gl_Normal, 0.0);
   V_eye = -V_eye;
   T_eye = vec4(gl_NormalMatrix * tangent.xyz, tangent.w);
   
   gl_TexCoord[0] = gl_MultiTexCoord0;
   gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}

