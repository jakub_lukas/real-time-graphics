varying vec4 V_eye;
varying vec4 L_eye;
varying vec4 N_eye;

uniform sampler2D diffuse_map;
uniform int texturing_enabled;
uniform float roughness;

// http://ruh.li/GraphicsOrenNayar.html

void main()
{   
    vec4 diffuse_material = gl_FrontMaterial.diffuse;
   if (texturing_enabled > 0)
       diffuse_material = texture2D(diffuse_map, gl_TexCoord[0].st);

    // interpolating normals will change the length of the normal, so renormalize the normal.
    vec4 N = normalize(N_eye);
    vec4 V = normalize(V_eye);
	vec4 L = normalize(L_eye);
    
    // calculate intermediary values
    float NdotL = dot(N, L);
    float NdotV = dot(N, V); 

    float angleVN = acos(NdotV);
    float angleLN = acos(NdotL);
    
    float alpha = max(angleVN, angleLN);
    float beta = min(angleVN, angleLN);
    float gamma = dot(V - N * dot(V, N), L - N * dot(L, N));
    
    float roughnessSquared = roughness * roughness;
    
    // calculate A and B
    float A = 1.0 - 0.5 * (roughnessSquared / (roughnessSquared + 0.57));
    float B = 0.45 * (roughnessSquared / (roughnessSquared + 0.09));
    float C = sin(alpha) * tan(beta);
    
    // put it all together
    float L1 = max(0.0, NdotL) * (A + B * max(0.0, gamma) * C);
    
    // get the final color 
    gl_FragColor = L1 * diffuse_material * gl_LightSource[0].diffuse;
}
