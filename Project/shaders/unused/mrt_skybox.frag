uniform samplerCube cube_map;
varying vec3 cube_map_coords;

void main(void)
{
   gl_FragData[0] = textureCube(cube_map, cube_map_coords);
   gl_FragData[1] = vec4(0.5 * cube_map_coords + 0.5, 1);
}
