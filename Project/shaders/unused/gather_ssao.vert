varying vec2 vTexCoord;

void main(void)
{
   vTexCoord = vec2(gl_Vertex);
   gl_Position = 2 * gl_Vertex - 1;
}

