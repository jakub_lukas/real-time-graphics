#version 150 compatibility

#ifdef GL_ES
	precision mediump float;
	precision mediump int;
	precision lowp sampler2D;
	precision lowp samplerCube;
#endif

struct Material {
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 emission;
	float shininess;
	//int texCount;
};

//////////////////////////////////////////////////////////////

uniform int texturing_enabled;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;


uniform sampler2D diffuse_map;

uniform Material material;

varying vec2 v_texcoord;

void main(void)
{
	vec4 diffuseMaterial = material.diffuse * ((1 - texturing_enabled) + texturing_enabled * texture(diffuse_map, v_texcoord));

	gl_FragColor = material.ambient + diffuseMaterial;
}
