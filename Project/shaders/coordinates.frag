#version 150 compatibility

#ifdef GL_ES
	precision mediump float;
	precision mediump int;
	precision lowp sampler2D;
	precision lowp samplerCube;
#endif

//////////////////////////////////////////////////////////////

varying vec4 color;

void main(void)
{
	gl_FragColor = color;
}

