#version 150 compatibility

#ifdef GL_ES
	precision mediump float;
	precision mediump int;
	precision lowp sampler2D;
	precision lowp samplerCube;
#endif

//////////////////////////////////////////////////////////////

uniform samplerCube cube_map;

varying vec3 cubeMapCoords;

void main(void)
{
   gl_FragColor = textureCube(cube_map, cubeMapCoords);
}
