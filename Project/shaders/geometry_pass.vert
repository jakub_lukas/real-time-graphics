uniform sampler2D displacement_map;
uniform int use_displacement;
uniform float displacement_factor;
uniform float near, far;

varying vec3 N_eye;
varying float linear_depth_eye;

void main(void)
{
   vec4 vertex = gl_Vertex;
   if (use_displacement > 0) {
	  float depth = texture2D(displacement_map, gl_MultiTexCoord0.st).r;
      vertex = gl_Vertex + displacement_factor * depth * vec4(gl_Normal, 0);
	}
	  
   N_eye = gl_NormalMatrix * gl_Normal;
   linear_depth_eye = (-(gl_ModelViewMatrix * vertex).z - near) / (far - near);
   gl_TexCoord[0] = gl_MultiTexCoord0;
   gl_Position = gl_ModelViewProjectionMatrix * vertex;
}

