uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

in vec3 a_position;
in float a_size;
in float a_life;


out float vSize;
out float vLife;

void main(void)
{
	vSize = a_size;
	vLife = a_life;
	gl_Position = viewMatrix * vec4(a_position, 1);
}
