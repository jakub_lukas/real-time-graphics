﻿using OpenTK.Graphics.OpenGL;

namespace Project.Scene
{
    class Mesh
    {
        public int VboVerticesId = 0;

        public int VboTexCoordsId = 0;

        public int VboNormalsId = 0;
	    public int VboTangentsId = 0;
	    public int VboBitangentsId = 0;

        public uint VboIndicesId = 0;

        public int NumFaces = 0;

        public int NumFaceIndices = 3; // pocet vrcholov kazdeho primitivu v meshi

        public string MaterialName = "";

        public void ClearGL()
        {
            if (VboVerticesId > 0)
                GL.DeleteBuffer(VboVerticesId);
            if (VboTexCoordsId > 0)
                GL.DeleteBuffer(VboTexCoordsId);
            if (VboNormalsId > 0)
                GL.DeleteBuffer(VboNormalsId);
            if (VboIndicesId > 0)
                GL.DeleteBuffer(VboIndicesId);
        }
    };
}

