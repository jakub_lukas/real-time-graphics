﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics;
using Project.Animations;
using Project.Helpers;
using Project.Scene.Cameras;
using Project.Scene.Helpers;
using Project.Scene.Lights;
using Project.Scene.Nodes;
using Project.Scene.ParticleSystem;

namespace Project.Scene
{
	class Scene
	{
		public List<Mesh> Meshes = new List<Mesh>();
		public List<LightNode> Lights = new List<LightNode>();
		public Dictionary<string, Camera> Cameras = new Dictionary<string, Camera>();
		public List<Animation> Animations = new List<Animation>();
		public List<Node> Nodes = new List<Node>();
		public Dictionary<string, Material> Materials = new Dictionary<string, Material>();
		public Camera MainCamera;
		public Mesh Skybox;
		
		public List<Emitter> ParticleSystems = new List<Emitter>();

		#region Construct / Destruct

		public Scene()
		{
			Materials.Add("Default", LoadDefaultMaterial());
			//Cameras.Add("Default", LoadDefaultCamera());
			//MainCamera = Cameras["Default"];
		}

		~Scene()
		{
			Meshes.Clear();
			Lights.Clear();
			Cameras.Clear();
			Animations.Clear();
			Nodes.Clear();
			Materials.Clear();
		}

		#endregion

		public void AddNode()
		{
			throw new NotImplementedException();
		}

		public void Update()
		{
			foreach (Camera cam in Cameras.Values)
				cam.Update();

			foreach (LightNode light in Lights)
				light.Update();

			foreach (Animation anim in Animations)
				anim.Update();

			foreach (Node node in Nodes)
				node.Update();

			foreach (Emitter emitter in ParticleSystems)
				emitter.Update(MainCamera.Position);
		}

		public static Scene MergeScenes(Scene scene1, Scene scene2)
		{
			var result = new Scene();

			// MATERIALS
			foreach (KeyValuePair<string, Material> material in scene1.Materials)
				result.Materials[material.Key] = material.Value;
			foreach (KeyValuePair<string, Material> material in scene2.Materials)
				result.Materials[material.Key] = material.Value;

			// MESHES
			foreach (Mesh mesh in scene1.Meshes)
				result.Meshes.Add(mesh);
			foreach (Mesh mesh in scene2.Meshes)
				result.Meshes.Add(mesh);

			// NODES
			foreach (Node node in scene1.Nodes)
				result.Nodes.Add(node);
			foreach (Node node in scene2.Nodes)
				result.Nodes.Add(node);

			// MAIN CAMERA
			if (scene1.MainCamera != null)
				result.MainCamera = scene1.MainCamera;
			else if (scene2.MainCamera != null)
				result.MainCamera = scene2.MainCamera;

			// LIGHTS
			foreach (LightNode light in scene1.Lights)
				result.Nodes.Add(light);
			foreach (LightNode light in scene2.Lights)
				result.Nodes.Add(light);

			// PARTICLE SYSTEMS
			foreach (Emitter pSys in scene1.ParticleSystems)
				result.Nodes.Add(pSys);
			foreach (Emitter pSys in scene2.ParticleSystems)
				result.Nodes.Add(pSys);

			return result;
		}

		#region Helpers

		private static Material LoadDefaultMaterial()
		{
			TextureManager.AddTexture("Default", Texture.Directory, "white.png");

			var material = new Material
			{
				ColorAmbient = new Color4(.2f, .2f, .2f, 1f),
				ColorDiffuse = new Color4(.6f, .6f, .6f, 1f),
				ColorSpecular = new Color4(1f, 1f, 1f, 1f),
				ColorEmission = new Color4(0f, 0f, 0f, 1f),
				Shininess = 16f,
				DiffuseTextureGLId = TextureManager.GetTexture("Default")
			};

			return material;
		}

		private static Camera LoadDefaultCamera()
		{
			return new FpsCamera(new Vector3(0, 0.5f, 8), -Vector3.UnitZ)
			{
				MovementFocus = true
			};
		}

		#endregion
	}
}
