﻿using OpenTK;
using OpenTK.Graphics;
using Project.Helpers;

namespace Project.Scene.Lights
{
	public enum LightType { Point, Directional, Spot }

	class LightNode : Nodes.Node
	{
		public LightType Type;
		public Color4 Ambient = new Color4(0.2f, 0.2f, 0.2f, 1.0f);
		public Color4 Diffuse = new Color4(1.0f, 1.0f, 1.0f, 1.0f);
		public Color4 Specular = new Color4(0.6f, 0.6f, 0.6f, 1.0f);
		public bool Active = true;

		public LightNode()
		{}

		public LightNode(Vector3 position, Vector3 forward, Vector3 up)
		{
			Position = position;
			Rotation = Matrix4.LookAt(position, forward, up).Inverted().ExtractRotation();
		}

		public void SetGL(int lightIndex, int shaderId)
		{
			Shader.SetUniform4(shaderId, "lightSource[" + lightIndex + "].ambient", Ambient);
			Shader.SetUniform4(shaderId, "lightSource[" + lightIndex + "].diffuse", Diffuse);
			Shader.SetUniform4(shaderId, "lightSource[" + lightIndex + "].specular", Specular);
			Shader.SetUniform1(shaderId, "lightSource[" + lightIndex + "].constantAttenuation", 1f);
			Shader.SetUniform1(shaderId, "lightSource[" + lightIndex + "].linearAttenuation", 1f);
			Shader.SetUniform1(shaderId, "lightSource[" + lightIndex + "].quadraticAttenuation", 1f);

			Vector4 pos;
			if (Animation == null)
				pos = (Type == LightType.Directional) ? new Vector4(Position, 0f) : new Vector4(Position, 1f);
			else
				pos = new Vector4(Animation.GetPosition(), 1f);
			Shader.SetUniform4(shaderId, "lightSource[" + lightIndex + "].position", pos);
		}

		public static void SetDefaultGL(int lightIndex, int shaderId)
		{
			Shader.SetUniform4(shaderId, "lightSource[" + lightIndex + "].ambient", Vector4.Zero);
			Shader.SetUniform4(shaderId, "lightSource[" + lightIndex + "].diffuse", Vector4.Zero);
			Shader.SetUniform4(shaderId, "lightSource[" + lightIndex + "].specular", Vector4.Zero);
			Shader.SetUniform1(shaderId, "lightSource[" + lightIndex + "].constantAttenuation", 0f);
			Shader.SetUniform1(shaderId, "lightSource[" + lightIndex + "].linearAttenuation", 0f);
			Shader.SetUniform1(shaderId, "lightSource[" + lightIndex + "].quadraticAttenuation", 0f);
			Shader.SetUniform4(shaderId, "lightSource[" + lightIndex + "].position", Vector4.Zero);
			Shader.SetUniformMatrix4(shaderId, "lightSource[" + lightIndex + "].viewMatrix", false, Matrix4.Identity);
			Shader.SetUniformMatrix4(shaderId, "lightSource[" + lightIndex + "].projectionMatrix", false, Matrix4.Identity);
			Shader.SetUniform1(shaderId, "lightSource[" + lightIndex + "].hasShadowMap", 0);
		}
	}
}
