﻿using System.Collections.Generic;
using OpenTK;
using Project.Scene.Helpers;

namespace Project.Scene.Nodes
{
	class Node : Movable
	{
		#region Properties

		public Mesh Mesh = null; // mesh(e) daneho node-u
		public List<Node> ChildrenNodes = new List<Node>(); // potomkovia node-u

		public override Matrix4 ModelMatrix
		{
			get
			{
				if (Animation != null)
					return Animation.GetMatrix();
				return Matrix4.CreateScale(Scale) * Matrix4.CreateFromQuaternion(Rotation) * Matrix4.CreateTranslation(Position);
			}
		}

		#endregion

		#region Setters

		public void SetModelMatrix(Matrix4 mat)
		{
			Position = mat.ExtractTranslation();
			Rotation = mat.ExtractRotation();
			Rotation.Normalize();
			Scale = mat.ExtractScale();
		}

		#endregion

		#region Updates

		public override void Update()
		{
			base.Update();

			foreach (Node node in ChildrenNodes)
				node.Update();
		}

		#endregion
	}
}
