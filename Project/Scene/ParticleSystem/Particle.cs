﻿using OpenTK;

namespace Project.Scene.ParticleSystem
{
	class Particle
	{
		public Vector3 Position;
		public Vector3 Speed;
		//public Color4 Color;
		public float Size;//, angle, weight;
		public float Life = -1f;
		public float CameraDistance = -1f;
	}
}
