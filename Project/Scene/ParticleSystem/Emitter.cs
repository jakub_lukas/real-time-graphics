﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Project.Helpers;
using Project.Scene.Nodes;

namespace Project.Scene.ParticleSystem
{
	class Emitter : Node
	{
		public static readonly Vector3 Gravity = new Vector3(0f, -9.81f, 0f);
		public const int MaxParticleCount = 1000;
		public int ParticleCount { get; protected set; }
		private int _lastUsedIndex = 0;
		public bool IsPlaying { get; private set; }

		public Vector3 InitSpeed = Vector3.Zero;
		public float EmitMinLife = 1f;
		public float EmitMaxLife = 2f;
		public float EmitMinSize = 1f;
		public float EmitMaxSize = 2f;
		public float GravityMultiplier = 1f;
		public float EmitSpeed = 10f;

		public int DiffuseTextureGLId = 0;
		public int DiffuseTexture2GLId = 0;

		public uint ParticlesPositionBuffer;
		private readonly float[] _particlesPositionBufferData = new float[(3 + 1 + 1) * MaxParticleCount];

		readonly Particle[] _particles = new Particle[MaxParticleCount];

		public Emitter()
		{
			for (int i = 0; i < MaxParticleCount; i++)
				_particles[i] = new Particle();
		}

		public Particle GetUnusedParticle()
		{
			for (int i = _lastUsedIndex; i < MaxParticleCount; i++)
			{
				if (_particles[i].Life < 0f)
				{
					_lastUsedIndex = i;
					return _particles[_lastUsedIndex];
				}
			}

			for (int i = 0; i < _lastUsedIndex; i++)
			{
				if (_particles[i].Life < 0f)
				{
					_lastUsedIndex = i;
					return _particles[_lastUsedIndex];
				}
			}

			return _particles[0];
		}

		private void EmitNewParticles()
		{
			int newCount = (int)Math.Ceiling(EmitSpeed * FpsCounter.DeltaTime);
			var random = new Random();

			for (int i = 0; i < newCount; i++)
			{
				var p = GetUnusedParticle();
				p.Position = Position;
				p.Life = (float)random.NextDouble() * (EmitMaxLife - EmitMinLife) + EmitMaxLife;
				p.Size = (float)random.NextDouble() * (EmitMaxSize - EmitMinSize) * EmitMinSize;
				p.Speed = new Vector3(InitSpeed.X + (float)random.NextDouble() - 0.5f, InitSpeed.Y, InitSpeed.Z + (float)random.NextDouble() - 0.5f);
				p.CameraDistance = -1f;
			}
		}

		public void Update(Vector3 cameraPosition)
		{
			EmitNewParticles();

			Array.Sort(_particles, (p1, p2) => p2.CameraDistance.CompareTo(p1.CameraDistance));

			ParticleCount = 0;
			for (int i = 0; i < MaxParticleCount; i++)
			{
				Particle p = _particles[i]; // shortcut

				if (p.Life < 0.0f)
				{
					p.CameraDistance = -1f;
					continue;
				};

				p.Life -= FpsCounter.DeltaTime; // Decrease life

				// Simulate simple physics : gravity only, no collisions
				p.Speed += Gravity * FpsCounter.DeltaTime * GravityMultiplier;
				p.Position += p.Speed * FpsCounter.DeltaTime;
				p.CameraDistance = (p.Position - cameraPosition).LengthSquared;
 
				// Fill the GPU buffer
				_particlesPositionBufferData[5 * ParticleCount + 0] = p.Position.X;
				_particlesPositionBufferData[5 * ParticleCount + 1] = p.Position.Y;
				_particlesPositionBufferData[5 * ParticleCount + 2] = p.Position.Z;
				_particlesPositionBufferData[5 * ParticleCount + 3] = p.Size;
				_particlesPositionBufferData[5 * ParticleCount + 4] = p.Life / EmitMaxLife;

				ParticleCount++;
			}

			UpdateGL();
		}

		private void UpdateGL()
		{
			GL.BindBuffer(BufferTarget.ArrayBuffer, ParticlesPositionBuffer);
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(MaxParticleCount * 5 * sizeof(float)), _particlesPositionBufferData, BufferUsageHint.StreamDraw); // Buffer orphaning, a common way to improve streaming perf. See above link for details.
			//GL.BufferSubData(BufferTarget.ArrayBuffer, IntPtr.Zero, (IntPtr)(ParticleCount * 4 * sizeof(float)), _particlesPositionBufferData);
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
		}

		public static Emitter CreateEmitter(Vector3 position, Vector3 speed, string texture1, string texture2, float gravityMultiplier)
		{
			var emitter = new Emitter() { Position = position, InitSpeed = speed, GravityMultiplier = gravityMultiplier };

			// The VBO containing the positions and sizes of the particles
			GL.GenBuffers(1, out emitter.ParticlesPositionBuffer);
			GL.BindBuffer(BufferTarget.ArrayBuffer, emitter.ParticlesPositionBuffer);
			// Initialize with empty (NULL) buffer : it will be updated later, each frame.
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(MaxParticleCount * 4 * sizeof(float)), IntPtr.Zero, BufferUsageHint.StreamDraw);

			emitter.DiffuseTextureGLId = Texture.Load(texture1);
			emitter.DiffuseTexture2GLId = Texture.Load(texture2);

			return emitter;
		}

		#region Play controls

		public void Play()
		{
			IsPlaying = true;
		}

		public void Pause()
		{
			IsPlaying = false;
		}

		public void Stop()
		{
			IsPlaying = false;
			for (int i = 0; i < MaxParticleCount; i++)
			{
				_particles[i].Life = -1;
			}
		}

		#endregion
	}
}
