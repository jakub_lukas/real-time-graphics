﻿using System;
using System.Windows.Forms;
using OpenTK;
using Project.Inputs;

namespace Project.Scene.Cameras
{
    class OrbitCamera : Camera
    {
        #region Properties

        public float Distance = 0.0f;
		public float Theta = 0.0f; // <0, 180> inclination
        public float Phi = 0.0f; // <0, 360> azimuth

		public override Matrix4 ModelMatrix
		{
			get
			{
				Matrix4 model = Matrix4.LookAt(Position, Vector3.Zero, Vector3.UnitY).Inverted();
				return (Animation != null) ? model * Animation.GetMatrix() : model;
			}
		}

        #endregion

        #region Constructors

		public OrbitCamera(float distance, float phi, float theta)
        {
			Phi = phi;
            Theta = theta;
            Distance = distance;

			UpdatePosition();
        }

        #endregion

		public void ChangeRotation(float phiInc, float thetaInc)
        {
			Phi += phiInc;
            Theta += thetaInc;

			UpdatePosition();
        }

        public void ChangeDistance(float distanceInc)
        {
            Distance += distanceInc;

			UpdatePosition();
        }

        #region Updates

		protected void UpdatePosition()
        {
			var phiRad = (float)(Math.PI * Phi / 180.0);
            var thetaRad = (float)(Math.PI * Theta / 180.0);

            Position = new Vector3
            {
				X = (float)(Distance * Math.Sin(thetaRad) * Math.Sin(phiRad)),
				Y = (float)(Distance * Math.Cos(thetaRad)),
				Z = (float)(Distance * Math.Sin(thetaRad) * Math.Cos(phiRad))
            };
        }

        protected override void UpdateMovement()
        {
            if (Input.GetMouse(MouseButtons.Right))
                ChangeRotation(-Input.GetAxis(Input.Axis.Horizontal), Input.GetAxis(Input.Axis.Vertical));

            ChangeDistance(-Input.GetMouseWheel());
        }

        #endregion
    }
}
