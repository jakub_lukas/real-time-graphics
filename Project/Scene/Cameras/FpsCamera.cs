﻿using OpenTK;

namespace Project.Scene.Cameras
{
	class FpsCamera : Camera
	{
		#region #region Constructors

		public FpsCamera()
			: base()
		{
		}

		public FpsCamera(Vector3 position)
			: base(position)
		{
		}

		public FpsCamera(Vector3 position, Vector3 forward)
			: base(position, forward)
		{
		}

		#endregion
	}
}
