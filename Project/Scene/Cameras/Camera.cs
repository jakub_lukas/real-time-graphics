﻿using OpenTK;
using Project.Scene.Helpers;

namespace Project.Scene.Cameras
{
	class Camera : Movable
	{
		#region Properties

		private float _ratio;

		public float NearPlane = 1f;
		public float FarPlane = 1000f;
		public Matrix4 ViewMatrix { get { return ModelMatrix.Inverted(); } }
		public Matrix4 ProjectionMatrix { get; private set; }

		#endregion

		#region Constructors

		public Camera()
		{
			ProjectionMatrix = Matrix4.Identity;
		}

		public Camera(Vector3 position)
			: base(position)
		{
            
		}

		public Camera(Vector3 position, Vector3 forward)
			: base(position, forward)
		{

		}

		#endregion

		public void SetAspectRatio(int width, int height)
		{
			_ratio = width / (float)height;

			ProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver2, _ratio, NearPlane, FarPlane);
		}
	}
}
