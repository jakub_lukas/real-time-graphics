﻿using Project.Helpers;
using Project.Scene.Helpers;

namespace Project.Scene.Primitives
{
	class Skybox
	{
		public const string Directory = "skybox";

		public static Material CreateMaterial()
		{
			return new Material
			{
				DiffuseTextureGLId = Texture.LoadCube(Directory + "/snow", "posX.jpg", "negX.jpg",
					"posY.jpg", "negY.jpg",
					"posZ.jpg", "negZ.jpg")
			};
		}

		public static Mesh CreateMesh(float size = 1f)
		{
			return Cube.CreateMesh(size);
		}
	}
}
