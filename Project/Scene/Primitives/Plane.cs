﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Project.Scene.Nodes;

namespace Project.Scene.Primitives
{
	class Plane
	{
		public static Mesh CreateMesh(float size = 1f)
		{
			float[] vertices = { -0.5f, 0f, 0.5f,   0.5f, 0f, 0.5f,   0.5f, 0, -0.5f,   -0.5f, 0f, -0.5f };
			for (int i = 0; i < vertices.Length; i++)
				vertices[i] *= size;

			float[] normals = { 0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0 };
			float[] tangents = { 1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0 };
			float[] bitangents = { 0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0 };

			float[] texcoords = { 0, 0,   1, 0,   1, 1,   1, 0 };

			uint[] indices = { 0, 1, 2, 3 };

			var mesh = new Mesh();

			// nacitaj vrcholy do OpenGL
			GL.GenBuffers(1, out mesh.VboVerticesId);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboVerticesId);
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * sizeof(float)), vertices, BufferUsageHint.StaticDraw);

			// nacitaj normaly
			GL.GenBuffers(1, out mesh.VboNormalsId);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboNormalsId);
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(normals.Length * sizeof(float)), normals, BufferUsageHint.StaticDraw);

			// nacitaj tangenty
			GL.GenBuffers(1, out mesh.VboTangentsId);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboTangentsId);
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(tangents.Length * sizeof(float)), tangents, BufferUsageHint.StaticDraw);

			// nacitaj bitangenty
			GL.GenBuffers(1, out mesh.VboBitangentsId);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboBitangentsId);
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(bitangents.Length * sizeof(float)), bitangents, BufferUsageHint.StaticDraw);

			// nacitaj texturne suradnice
			GL.GenBuffers(1, out mesh.VboTexCoordsId);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboTexCoordsId);
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(texcoords.Length * sizeof(float)), texcoords, BufferUsageHint.StaticDraw);

			mesh.NumFaceIndices = 4;
			mesh.NumFaces = 1;

			GL.GenBuffers(1, out mesh.VboIndicesId);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, mesh.VboIndicesId);
			GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(indices.Length * sizeof(uint)), indices, BufferUsageHint.StaticDraw);

			return mesh;
		}

		public static Node CreateNode(Vector3 point, Vector3 normal)
		{
			var node = new Node
			{
				Mesh = CreateMesh(),
				Position = point,
				Scale = new Vector3(normal.Length)
			};

			var axis = Vector3.Cross(Vector3.UnitY, normal.Normalized());
			float angle = (float)Math.Acos(Vector3.Dot(Vector3.UnitY, normal.Normalized()));
			node.Rotation = Quaternion.FromAxisAngle(axis, angle);
			return node;
		}
	}
}
