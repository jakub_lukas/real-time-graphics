﻿using System;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;
using Project.Scene.Nodes;

namespace Project.Scene.Primitives
{
	class Cube
	{
		public static Mesh CreateMesh(float size = 1f)
		{
			float[] vertices = { 1, 1, 1,   -1, 1, 1,   -1,-1, 1,   1,-1, 1,  
								 1, 1, 1,    1,-1, 1,    1,-1,-1,   1, 1,-1,
								 1, 1, 1,    1, 1,-1,   -1, 1,-1,  -1, 1, 1,
							    -1, 1, 1,   -1, 1,-1,   -1,-1,-1,  -1,-1, 1,
							    -1,-1,-1,    1,-1,-1,    1,-1, 1,  -1,-1, 1,
								 1,-1,-1,   -1,-1,-1,   -1, 1,-1,   1, 1,-1 };
			for (int i = 0; i < vertices.Length; i++)
				vertices[i] *= size;

			float[] normals = { 0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1,  
								1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0,
								0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0,
							   -1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0,
								0,-1, 0,   0,-1, 0,   0,-1, 0,   0,-1, 0,
								0, 0,-1,   0, 0,-1,   0, 0,-1,   0, 0,-1};

			float[] texcoords = { 1, 1,   0, 1,   0, 0,   1, 0,
								  0, 1,   0, 0,   1, 0,   1, 1,
								  1, 0,   1, 1,   0, 1,   0, 0,
								  1, 1,   0, 1,   0, 0,   1, 0,
								  0, 0,   1, 0,   1, 1,   0, 1,
								  0, 0,   1, 0,   1, 1,   0, 1};

			uint[] indices = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 };

			var mesh = new Mesh();

			// nacitaj vrcholy do OpenGL
			GL.GenBuffers(1, out mesh.VboVerticesId);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboVerticesId);
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * sizeof(float)), vertices, BufferUsageHint.StaticDraw);

			// nacitaj normaly
			GL.GenBuffers(1, out mesh.VboNormalsId);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboNormalsId);
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(normals.Length * sizeof(float)), normals, BufferUsageHint.StaticDraw);

			// nacitaj texturne suradnice
			GL.GenBuffers(1, out mesh.VboTexCoordsId);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboTexCoordsId);
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(texcoords.Length * sizeof(float)), texcoords, BufferUsageHint.StaticDraw);

			mesh.NumFaceIndices = 4;
			mesh.NumFaces = 6;

			GL.GenBuffers(1, out mesh.VboIndicesId);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, mesh.VboIndicesId);
			GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(indices.Length * sizeof(uint)), indices, BufferUsageHint.StaticDraw);

			return mesh;
		}

		public static Node CreateNode(List<Mesh> meshes)
		{
			var node = new Node { Mesh = CreateMesh() };
			meshes.Add(node.Mesh);

			return node;
		}
	}
}
