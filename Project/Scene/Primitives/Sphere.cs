﻿using System;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;
using Project.Scene.Nodes;

namespace Project.Scene.Primitives
{
	class Sphere
	{
		public static Mesh CreateMesh(uint xSlices, uint ySlices, float diameter = 1f)
		{
			var vertices = new float[3 * (xSlices + 1) * (ySlices + 1)];
			var normals = new float[3 * (xSlices + 1) * (ySlices + 1)];
			var texcoords = new float[2 * (xSlices + 1) * (ySlices + 1)];

			int index;
			for (uint i = 0; i <= xSlices; i++)
			{
				float u = i / (float)xSlices;
				for (uint j = 0; j <= ySlices; j++)
				{
					float v = j / (float)ySlices;

					index = (int)(i + j * (xSlices + 1));

					normals[3 * index + 0] = (float)(Math.Cos(2 * u * Math.PI) * Math.Cos((v - 0.5f) * Math.PI));
					normals[3 * index + 1] = (float)(Math.Sin(2 * u * Math.PI) * Math.Cos((v - 0.5f) * Math.PI));
					normals[3 * index + 2] = (float)(Math.Sin((v - 0.5f) * Math.PI));

					vertices[3 * index + 0] = normals[3 * index + 0] * diameter;
					vertices[3 * index + 1] = normals[3 * index + 1] * diameter;
					vertices[3 * index + 2] = normals[3 * index + 2] * diameter;

					texcoords[2 * index + 0] = u;
					texcoords[2 * index + 1] = 1.0f - v;
				}
			}

			var indices = new uint[4 * (xSlices + 0) * (ySlices + 0)];
			index = 0;
			for (uint i = 0; i < xSlices; i++)
			{
				for (uint j = 0; j < ySlices; j++)
				{
					indices[index] = i + j * (xSlices + 1);
					index++;
					indices[index] = (i + 1) + j * (xSlices + 1);
					index++;
					indices[index] = (i + 1) + (j + 1) * (xSlices + 1);
					index++;
					indices[index] = i + (j + 1) * (xSlices + 1);
					index++;
				}
			}

			var mesh = new Mesh();

			GL.GenBuffers(1, out mesh.VboVerticesId);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboVerticesId);
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * sizeof(float)), vertices, BufferUsageHint.StaticDraw);

			GL.GenBuffers(1, out mesh.VboNormalsId);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboNormalsId);
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(normals.Length * sizeof(float)), normals, BufferUsageHint.StaticDraw);

			GL.GenBuffers(1, out mesh.VboTexCoordsId);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboTexCoordsId);
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(texcoords.Length * sizeof(float)), texcoords, BufferUsageHint.StaticDraw);

			mesh.NumFaceIndices = 4;
			mesh.NumFaces = (int)(xSlices * ySlices);

			GL.GenBuffers(1, out mesh.VboIndicesId);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, mesh.VboIndicesId);
			GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(indices.Length * sizeof(uint)), indices, BufferUsageHint.StaticDraw);

			return mesh;
		}

		public static Node CreateNode(List<Mesh> meshes)
		{
			var node = new Node { Mesh = CreateMesh(12, 12) };
			meshes.Add(node.Mesh);

			return node;
		}
	}
}
