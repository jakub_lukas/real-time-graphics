﻿using System;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;
using Project.Scene.Nodes;

namespace Project.Scene.Primitives
{
    class Torus
    {
        public static Mesh CreateMesh(float innerRadius, float outerRadius, uint xSlices, uint ySlices)
        {
            var vertices = new float[3 * (xSlices + 1) * (ySlices + 1)];
            var normals = new float[3 * (xSlices + 1) * (ySlices + 1)];
            var texcoords = new float[2 * (xSlices + 1) * (ySlices + 1)];

            int index;
            for (uint i = 0; i <= xSlices; i++)
            {
                float u = i / (float)xSlices;
                for (uint j = 0; j <= ySlices; j++)
                {
                    float v = j / (float)ySlices;

                    index = (int)(i + j * (xSlices + 1));

                    vertices[3 * index + 0] = (float)(Math.Cos(2 * u * Math.PI) * (outerRadius + innerRadius * Math.Cos(2 * v * Math.PI)));
                    vertices[3 * index + 1] = (float)(Math.Sin(2 * u * Math.PI) * (outerRadius + innerRadius * Math.Cos(2 * v * Math.PI)));
                    vertices[3 * index + 2] = (float)(innerRadius * Math.Sin(2 * v * Math.PI));

                    float tx = (float)(-Math.Sin(2 * u * Math.PI));
                    float ty = (float)(Math.Cos(2 * u * Math.PI));
                    float tz = 0f;
                    float sx = (float)(Math.Cos(2 * u * Math.PI) * (-Math.Sin(2 * v * Math.PI)));
                    float sy = (float)(Math.Sin(2 * u * Math.PI) * (-Math.Sin(2 * v * Math.PI)));
                    float sz = (float)(Math.Cos(2 * v * Math.PI));
                    normals[3 * index + 0] = ty * sz - tz * sy;
                    normals[3 * index + 1] = tz * sx - tx * sz;
                    normals[3 * index + 2] = tx * sy - ty * sx;

                    texcoords[2 * index + 0] = u;
                    texcoords[2 * index + 1] = 1.0f - v;
                }
            }

            var indices = new uint[4 * (xSlices + 0) * (ySlices + 0)];
            index = 0;
            for (uint i = 0; i < xSlices; i++)
            {
                for (uint j = 0; j < ySlices; j++)
                {
                    indices[index] = i + j * (xSlices + 1);
                    index++;
                    indices[index] = (i + 1) + j * (xSlices + 1);
                    index++;
                    indices[index] = (i + 1) + (j + 1) * (xSlices + 1);
                    index++;
                    indices[index] = i + (j + 1) * (xSlices + 1);
                    index++;
                }
            }

            var mesh = new Mesh();

            GL.GenBuffers(1, out mesh.VboVerticesId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboVerticesId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * sizeof(float)), vertices, BufferUsageHint.StaticDraw);

            GL.GenBuffers(1, out mesh.VboNormalsId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboNormalsId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(normals.Length * sizeof(float)), normals, BufferUsageHint.StaticDraw);

            GL.GenBuffers(1, out mesh.VboTexCoordsId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboTexCoordsId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(texcoords.Length * sizeof(float)), texcoords, BufferUsageHint.StaticDraw);

            mesh.NumFaceIndices = 4;
            mesh.NumFaces = (int)(xSlices * ySlices);

            GL.GenBuffers(1, out mesh.VboIndicesId);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, mesh.VboIndicesId);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(indices.Length * sizeof(uint)), indices, BufferUsageHint.StaticDraw);

            return mesh;
        }

        public static Node CreateNode(List<Mesh> meshes)
        {
            var node = new Node { Mesh = CreateMesh(0.6f, 2.0f, 12, 12) };
            meshes.Add(node.Mesh);

            return node;
        }
    }
}
