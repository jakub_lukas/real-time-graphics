﻿using OpenTK.Graphics;
using Project.Helpers;

namespace Project.Scene.Helpers
{
	class Material
	{
		public int DiffuseTextureGLId = 0;
		public int NormalTextureGLId = 0;
		public Color4 ColorAmbient = new Color4(.2f, .2f, .2f, 1f);
		public Color4 ColorDiffuse = new Color4(.8f, .8f, .8f, 1f);
		public Color4 ColorSpecular = new Color4(0f, 0f, 0f, 1f);
		public Color4 ColorEmission = new Color4(0f, 0f, 0f, 1f);
		public float Shininess = 16;

		public string Shader = "";

		public void ClearGL()
		{
			if (DiffuseTextureGLId > 0)
				Texture.Unload(DiffuseTextureGLId);
			if (NormalTextureGLId > 0)
				Texture.Unload(NormalTextureGLId);
		}
	}
}
