﻿using System;
using OpenTK.Graphics.OpenGL;

namespace Project.Scene.Helpers
{
	class Fbo
	{
		public uint Id; // OpenGL ID framebuffer objektu
		public uint[] ColorTextures = new uint[4]; // id textur pripojenych k FBO, zatial napevno 4
		public uint DepthTexture; // id textury pre depth buffer

		public Fbo()
		{
			Id = 0;
			ColorTextures[0] = 0;
			ColorTextures[1] = 0;
			ColorTextures[2] = 0;
			ColorTextures[3] = 0;
			DepthTexture = 0;
		}

		// vymaz framebuffer a jeho textury z OpenGL
		public void Clear()
		{
			if (DepthTexture > 0) GL.DeleteTexture(DepthTexture);
			if (ColorTextures[0] > 0) GL.DeleteTexture(ColorTextures[0]);
			if (ColorTextures[1] > 0) GL.DeleteTexture(ColorTextures[1]);
			if (ColorTextures[2] > 0) GL.DeleteTexture(ColorTextures[2]);
			if (ColorTextures[3] > 0) GL.DeleteTexture(ColorTextures[3]);
			if (Id > 0) GL.DeleteFramebuffer(Id);

			Id = 0;
			ColorTextures[0] = 0;
			ColorTextures[1] = 0;
			ColorTextures[2] = 0;
			ColorTextures[3] = 0;
			DepthTexture = 0;
		}

		public static void CheckFramebufferStatus() //TODO: nide idea, maybe class of OGL checkers ?? "nice" static everywherecallable functions ?
		{
			switch (GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer))
			{
				case FramebufferErrorCode.FramebufferComplete:
					{
						Console.WriteLine(@"FBO: The framebuffer is complete and valid for rendering.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteAttachment:
					{
						Console.WriteLine(@"FBO: One or more attachment points are not framebuffer attachment complete. This could mean there’s no texture attached or the format isn’t renderable. For color textures this means the base format must be RGB or RGBA and for depth textures it must be a DEPTH_COMPONENT format. Other causes of this error are that the width or height is zero or the z-offset is out of range in case of render to volume.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteMissingAttachment:
					{
						Console.WriteLine(@"FBO: There are no attachments.");
						break;
					}
				/* case  FramebufferErrorCode.GL_FRAMEBUFFER_INCOMPLETE_DUPLICATE_ATTACHMENT_EXT: 
					 {
						 Console.WriteLine("FBO: An object has been attached to more than one attachment point.");
						 break;
					 }*/
				case FramebufferErrorCode.FramebufferIncompleteDimensionsExt:
					{
						Console.WriteLine(@"FBO: Attachments are of different size. All attachments must have the same width and height.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteFormatsExt:
					{
						Console.WriteLine(@"FBO: The color attachments have different format. All color attachments must have the same format.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteDrawBuffer:
					{
						Console.WriteLine(@"FBO: An attachment point referenced by GL.DrawBuffers() doesn’t have an attachment.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteReadBuffer:
					{
						Console.WriteLine(@"FBO: The attachment point referenced by GL.ReadBuffers() doesn’t have an attachment.");
						break;
					}
				case FramebufferErrorCode.FramebufferUnsupported:
					{
						Console.WriteLine(@"FBO: This particular FBO configuration is not supported by the implementation.");
						break;
					}
				default:
					{
						Console.WriteLine(@"FBO: Status unknown. (yes, this is really bad.)");
						break;
					}
			}
		}

		// vytvor a inicializuj zakladne fbo s niekolkymi color buframi a depth bufrom
		public static Fbo CreateFbo(int width, int height, int numColorBuffers, bool depthBuffer)
		{
			if (numColorBuffers > 4)
				numColorBuffers = 4;

			int maxColorAttachments;
			GL.GetInteger(GetPName.MaxColorAttachments, out maxColorAttachments);
			if (numColorBuffers > maxColorAttachments)
				numColorBuffers = maxColorAttachments;

			var fbo = new Fbo();
			GL.GenFramebuffers(1, out fbo.Id);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo.Id);

			// vytvor dany pocet texture a pripoj ich ku color bufrom
			for (int i = 0; i < numColorBuffers; i++)
			{
				var attachment = FramebufferAttachment.ColorAttachment0;
				switch (i)
				{
					case 0: attachment = FramebufferAttachment.ColorAttachment0;
						break;
					case 1: attachment = FramebufferAttachment.ColorAttachment1;
						break;
					case 2: attachment = FramebufferAttachment.ColorAttachment2;
						break;
					case 3: attachment = FramebufferAttachment.ColorAttachment3;
						break;
				}

				GL.GenTextures(1, out fbo.ColorTextures[i]);
				GL.BindTexture(TextureTarget.Texture2D, fbo.ColorTextures[i]);

				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
				GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba8, width, height, 0, PixelFormat.Rgba, PixelType.UnsignedShort, IntPtr.Zero);
				GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, attachment, TextureTarget.Texture2D, fbo.ColorTextures[i], 0);
			}

			// pridaj texturu alebo renderbuffer ako depth buffer
			if (depthBuffer)
			{
				GL.GenTextures(1, out fbo.DepthTexture);
				GL.BindTexture(TextureTarget.Texture2D, fbo.DepthTexture);
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
				GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.DepthComponent, width, height, 0, PixelFormat.DepthComponent, PixelType.UnsignedShort, IntPtr.Zero);
				GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, TextureTarget.Texture2D, fbo.DepthTexture, 0);
			}

			// nastav buffre pre zapis
			if (numColorBuffers == 0)
				GL.DrawBuffer(DrawBufferMode.None);
			else if (numColorBuffers == 1)
				GL.DrawBuffer(DrawBufferMode.ColorAttachment0);
			else
			{
				DrawBuffersEnum[] attachments =
				{
					DrawBuffersEnum.ColorAttachment0,
					DrawBuffersEnum.ColorAttachment1,
					DrawBuffersEnum.ColorAttachment2,
					DrawBuffersEnum.ColorAttachment3
				};
				GL.DrawBuffers(numColorBuffers, attachments);
			}

			// Test for FBO Error
			CheckFramebufferStatus();

			GL.BindTexture(TextureTarget.Texture2D, 0);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
			return fbo;
		}

		// nastav view a projekcnu maticu na vyrenderovanie sceny spoza zrkadla, zrj=kadlo je dane ako stvoruholnik vo svetovych suradniciach
		//void SetMirrorMatrices(aiVector3D camera_pos, std::vector<aiVector3D> mirror_points);

		// vykresli polygon zrkadla s namapovanou texturou zrkadla
		//void RenderMirror(std::vector<aiVector3D> mirror_points, GLdouble mirror_view_matrix[16], GLdouble mirror_projection_matrix[16], GLuint mirror_tex);
	};
}
