﻿using System.Windows.Forms;
using OpenTK;
using Project.Animations;
using Project.Inputs;

namespace Project.Scene.Helpers
{
	class Movable
	{
		#region Properties

		private Vector3 _position = Vector3.Zero;
		public virtual Vector3 Position
		{
			get { return (Animation != null) ? Animation.GetPosition() : _position; }
			set { _position = value; }
		}

		private Quaternion _rotation = Quaternion.Identity;
		public virtual Quaternion Rotation
		{
			get { return (Animation != null) ? Animation.GetRotation() : _rotation; }
			set { _rotation = value; }
		}

		private Vector3 _scale = Vector3.Zero;
		public virtual Vector3 Scale
		{
			get { return (Animation != null) ? Animation.GetScale() : _scale; }
			set { _scale = value; }
		}

		public Vector3 AxisX { get { return Vector3.Transform(Vector3.UnitX, Rotation); } }
		public Vector3 AxisY { get { return Vector3.Transform(Vector3.UnitY, Rotation); } }
		public Vector3 AxisZ { get { return Vector3.Transform(Vector3.UnitZ, Rotation); } }

		public Vector3 Direction { get { return -AxisZ; } }

		public virtual Matrix4 ModelMatrix
		{
			get
			{
				Matrix4 model = Matrix4.CreateFromQuaternion(Rotation) * Matrix4.CreateTranslation(Position);// * Matrix4.CreateScale(Scale);
				if (Animation != null)
					return model*Animation.GetMatrix();
				return model;
			}
		}

		public const float SlowMultiplier = 0.1f; // TODO: user changable
		public const float SpeedMultiplier = 3f; // TODO: user changable

		public Animation Animation = null;

		public bool MovementFocus;

		#endregion

		#region Constructors

		public Movable() : this(Vector3.Zero) { }

		public Movable(Vector3 position) : this(position, (position != Vector3.Zero) ? (Vector3.Zero - position).Normalized() : -Vector3.UnitZ) { }

		public Movable(Vector3 position, Vector3 forward) : this(position, forward, Vector3.UnitY) { }

		public Movable(Vector3 position, Vector3 forward, Vector3 up)
		{
			Position = position;
			Rotation = Matrix4.LookAt(position, forward, up).Inverted().ExtractRotation();
			Scale = new Vector3(1f, 1f, 1f);
		}

		#endregion

		#region Handlers

		protected void Rotate(float rotateX, float rotateY)
		{
			float headingDegrees = -MathHelper.DegreesToRadians(rotateX);
			float pitchDegrees = MathHelper.DegreesToRadians(rotateY);

			var q1 = Quaternion.FromAxisAngle(Vector3.UnitY, headingDegrees);
			var q2 = Quaternion.FromAxisAngle(Vector3.UnitX, pitchDegrees);

			Rotation *= q1 * q2;

			Rotation = Matrix4.LookAt(Position, Position - AxisZ, Vector3.UnitY).Inverted().ExtractRotation();
		}

		#endregion

		#region Updates

		public virtual void Update()
		{
			if (MovementFocus)
				UpdateMovement();
		}

		protected virtual void UpdateMovement()
		{
			if (Input.GetMouse(MouseButtons.Right))
				Rotate(Input.GetAxis(Input.Axis.Horizontal), Input.GetAxis(Input.Axis.Vertical));

			if (Input.GetKey(Keys.W))
				MoveForward();

			if (Input.GetKey(Keys.S))
				MoveBack();

			if (Input.GetKey(Keys.A))
				MoveLeft();

			if (Input.GetKey(Keys.D))
				MoveRight();

			if (Input.GetKey(Keys.Space))
				MoveUp();

			if (Input.GetKey(Keys.ControlKey))
				MoveDown();
		}

		#endregion

		#region Movements

		public void MoveForward()
		{
			if (Input.GetKey(Keys.ShiftKey))
				Position -= AxisZ*SpeedMultiplier;
			else if (Input.GetKey(Keys.C))
				Position -= AxisZ*SlowMultiplier;
			else
				Position -= AxisZ;
		}

		public void MoveBack()
		{
			if (Input.GetKey(Keys.ShiftKey))
				Position += AxisZ * SpeedMultiplier;
			else if (Input.GetKey(Keys.C))
				Position += AxisZ * SlowMultiplier;
			else
				Position += AxisZ;
		}

		public void MoveRight()
		{
			if (Input.GetKey(Keys.ShiftKey))
				Position += AxisX * SpeedMultiplier;
			else if (Input.GetKey(Keys.C))
				Position += AxisX * SlowMultiplier;
			else
				Position += AxisX;
		}

		public void MoveLeft()
		{
			if (Input.GetKey(Keys.ShiftKey))
				Position -= AxisX * SpeedMultiplier;
			else if (Input.GetKey(Keys.C))
				Position -= AxisX * SlowMultiplier;
			else
				Position -= AxisX;
		}

		public void MoveUp()
		{
			if (Input.GetKey(Keys.ShiftKey))
				Position += Vector3.UnitY * SpeedMultiplier;
			else if (Input.GetKey(Keys.C))
				Position += Vector3.UnitY * SlowMultiplier;
			else
				Position += Vector3.UnitY;
		}

		public void MoveDown()
		{
			if (Input.GetKey(Keys.ShiftKey))
				Position -= Vector3.UnitY * SpeedMultiplier;
			else if (Input.GetKey(Keys.C))
				Position -= Vector3.UnitY * SlowMultiplier;
			else
				Position -= Vector3.UnitY;
		}

		#endregion
    }
}
