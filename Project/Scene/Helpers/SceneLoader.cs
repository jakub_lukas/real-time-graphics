﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Assimp;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Project.Extensions;
using Project.Helpers;
using Node = Project.Scene.Nodes.Node;

namespace Project.Scene.Helpers
{
	class SceneLoader
	{
		public const string Directory = "models";

		public static void LoadMaterials(List<Assimp.Material> aiMaterials, Scene scene, string dirPath = "")
		{
			//TODO: remove material duplicities
			var loadedTextures = new Dictionary<string, int>();

			foreach (Assimp.Material aiMaterial in aiMaterials)
			{
				var material = new Material();

				if (aiMaterial.HasColorAmbient)
					material.ColorAmbient = aiMaterial.ColorAmbient.ToColor4();

				if (aiMaterial.HasColorDiffuse)
					material.ColorDiffuse = aiMaterial.ColorDiffuse.ToColor4();

				if (aiMaterial.HasColorSpecular)
					material.ColorSpecular = aiMaterial.ColorSpecular.ToColor4();

				if (aiMaterial.HasColorEmissive)
					material.ColorEmission = aiMaterial.ColorEmissive.ToColor4();

				if (aiMaterial.HasShininess)
				{
					float shininess = Math.Max(aiMaterial.Shininess, 1f);
					if (aiMaterial.HasShininessStrength)
						material.Shininess = shininess * Math.Max(aiMaterial.ShininessStrength, 1f);
					else
						material.Shininess = shininess;
				}
				else
				{
					material.Shininess = 0f;
					material.ColorSpecular = new Color4(0f, 0f, 0f, 1f);
				}

				// nacitaj difuznu texturu
				if (aiMaterial.GetMaterialTextureCount(TextureType.Diffuse) > 0)
				{
					TextureSlot texSlot;
					aiMaterial.GetMaterialTexture(TextureType.Diffuse, 0, out texSlot);
					string path = texSlot.FilePath;

					int textureId;
					if (loadedTextures.TryGetValue(path, out textureId)) // texturu sme uz nacitali, iba pouzijeme uz existujuce id
					{
						material.DiffuseTextureGLId = textureId;
					}
					else
					{
						path = Texture.GetPath(dirPath, path);
						material.DiffuseTextureGLId = TextureManager.AddTexture(path, path);

						if (material.DiffuseTextureGLId > 0)
							loadedTextures.Add(path, material.DiffuseTextureGLId);
					}
				}

				if (aiMaterial.GetMaterialTextureCount(TextureType.Normals) > 0)
				{
					TextureSlot texSlot;
					aiMaterial.GetMaterialTexture(TextureType.Normals, 0, out texSlot);
					string path = texSlot.FilePath;

					int textureId;
					if (loadedTextures.TryGetValue(path, out textureId)) // texturu sme uz nacitali, iba pouzijeme uz existujuce id
					{
						material.NormalTextureGLId = textureId;
					}
					else
					{
						path = Texture.GetPath(dirPath, path);
						material.NormalTextureGLId = TextureManager.AddTexture(path, path);

						if (material.NormalTextureGLId > 0)
							loadedTextures.Add(path, material.NormalTextureGLId);
					}
				}

				scene.Materials.Add(aiMaterial.Name, material);
			}
		}

		public static void LoadMeshes(List<Assimp.Mesh> aiMeshes, List<Assimp.Material> aiMaterials, Scene scene)
		{
			foreach (Assimp.Mesh aiMesh in aiMeshes)
			{
				var mesh = new Mesh();

				// nastav material
				if (aiMesh.MaterialIndex > -1 && aiMesh.MaterialIndex < scene.Materials.Count())
					mesh.MaterialName = aiMaterials[aiMesh.MaterialIndex].Name;
				else
					Debug.WriteLine("Invalid Material ID");

				// nacitaj vrcholy do OpenGL
				if (aiMesh.HasVertices)
				{
					GL.GenBuffers(1, out mesh.VboVerticesId);
					GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboVerticesId);
					float[] vert = Vector3DExt.Vector3DArrayToGLArray(aiMesh.Vertices);
					GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vert.Count() * sizeof(float)), vert, BufferUsageHint.StaticDraw);
				}

				// nacitaj normaly
				if (aiMesh.HasNormals)
				{
					GL.GenBuffers(1, out mesh.VboNormalsId);
					GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboNormalsId);
					float[] norm = Vector3DExt.Vector3DArrayToGLArray(aiMesh.Normals);
					GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(norm.Count() * sizeof(float)), norm, BufferUsageHint.StaticDraw);
				}

				// nacitaj tangenty / bitangenty
				if (aiMesh.HasTangentBasis)
				{
					GL.GenBuffers(1, out mesh.VboTangentsId);
					GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboTangentsId);
					float[] tang = Vector3DExt.Vector3DArrayToGLArray(aiMesh.Tangents);
					GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(tang.Count() * sizeof(float)), tang, BufferUsageHint.StaticDraw);

					GL.GenBuffers(1, out mesh.VboBitangentsId);
					GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboBitangentsId);
					float[] bitang = Vector3DExt.Vector3DArrayToGLArray(aiMesh.BiTangents);
					GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(bitang.Count() * sizeof(float)), bitang, BufferUsageHint.StaticDraw);
				}

				// nacitaj texturne suradnice
				if (aiMesh.HasTextureCoords(0))
				{
					GL.GenBuffers(1, out mesh.VboTexCoordsId);
					GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboTexCoordsId);
					var texCoords = new float[2 * aiMesh.VertexCount];
					for (int j = 0; j < aiMesh.VertexCount; j++)
					{
						texCoords[2 * j] = aiMesh.TextureCoordinateChannels[0][j].X;
						texCoords[2 * j + 1] = 1 - aiMesh.TextureCoordinateChannels[0][j].Y;
					}
					GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(texCoords.Count() * sizeof(float)), texCoords, BufferUsageHint.StaticDraw);
				}

				// nacitaj indexy pre trojuholniky
				// najprv zisti pocet indexov pre jeden face
				// toto definuje ci sa jedna o samostatne body, usecky alebo trojuholniky
				mesh.NumFaceIndices = 0;
				for (int t = 0; t < aiMesh.FaceCount; t++)
				{
					if (mesh.NumFaceIndices < aiMesh.Faces[t].IndexCount)
						mesh.NumFaceIndices = aiMesh.Faces[t].IndexCount;
				}
				mesh.NumFaces = aiMesh.FaceCount;

				// vypln pole indexov
				var indices = new int[aiMesh.FaceCount * mesh.NumFaceIndices];
				for (int t = 0; t < indices.Count(); t++)
					indices[t] = 0;
				for (int t = 0; t < aiMesh.FaceCount; t++)
				{
					for (int j = 0; j < aiMesh.Faces[t].IndexCount; j++)
						indices[t * mesh.NumFaceIndices + j] = aiMesh.Faces[t].Indices[j];
				}

				GL.GenBuffers(1, out mesh.VboIndicesId);
				GL.BindBuffer(BufferTarget.ElementArrayBuffer, mesh.VboIndicesId);
				GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(aiMesh.FaceCount * mesh.NumFaceIndices * sizeof(uint)), indices, BufferUsageHint.StaticDraw);

				scene.Meshes.Add(mesh);
			}
		}

		public static void LoadNodes(Assimp.Node aiRootNode, Scene scene)
		{
			scene.Nodes.Add(LoadNode(aiRootNode, scene));
		}

		private static Node LoadNode(Assimp.Node aiNode, Scene scene)
		{
			var node = new Node
			{
				ChildrenNodes = aiNode.Children.Select(node1 => LoadNode(node1, scene)).ToList()
			};
			node.SetModelMatrix(aiNode.Transform.Transposed().ToMatrix4());

			foreach (int meshId in aiNode.MeshIndices)
			{
				if (meshId > -1 && meshId < scene.Meshes.Count())
					node.ChildrenNodes.Add(new Node() { Mesh = scene.Meshes[meshId] });
				else
					Debug.WriteLine("Invalid Mesh ID");
			}

			return node;
		}

		// nacitanie modelu do vnutornych OpenGL struktur z externeho suboru
		public static Scene Load(string modelFile)
		{
			Debug.WriteLine("Importing model from file \"" + Directory + "/" + modelFile + "\"");

			string dirPath = "";
			string fileName = Directory + "/" + modelFile;
			PrepareDirPathAndFileName(ref dirPath, ref fileName);

			// nacitaj scenu a zisti, ci sa nacitanie podarilo
			// pri nacitani sa aj optimalizuju a trianguluju vsetky meshe
			Assimp.Scene aiScene = null;
			try
			{
				var aImporter = new AssimpContext();
				aiScene = aImporter.ImportFile(dirPath + fileName, PostProcessPreset.TargetRealTimeMaximumQuality);
			}
			catch (Exception)
			{
				Debug.WriteLine("Import failed!");
			}
			if (aiScene == null)
			{
				Debug.WriteLine("Import failed!");
				return null;
			}

			var resultScene = new Scene();

			LoadMaterials(aiScene.Materials, resultScene, dirPath); // nacitaj vsetky materialy
			LoadMeshes(aiScene.Meshes, aiScene.Materials, resultScene); // nacitaj vsetky meshe do OpenGL ako VBO
			LoadNodes(aiScene.RootNode, resultScene);

			Debug.WriteLine("Import successfull");
			return resultScene;
		}

		//TODO: move to more appropriate place
		private static void PrepareDirPathAndFileName(ref string dirPath, ref string fileName)
		{
			var fileInfo = new FileInfo(fileName);
			if (fileInfo.Directory != null)
			{
				dirPath += fileInfo.Directory.FullName + "\\";
				fileName = fileInfo.Name;
			}
			else
			{
				dirPath = "";
			}
		}
	}
}
