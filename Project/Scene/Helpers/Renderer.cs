﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Project.Extensions;
using Project.Helpers;
using Project.Scene.Lights;
using Project.Scene.ParticleSystem;
using Node = Project.Scene.Nodes.Node;
using PrimitiveType = OpenTK.Graphics.OpenGL.PrimitiveType;

namespace Project.Scene.Helpers
{
	class Renderer
	{
		#region Attributes

		internal class LightCache
		{
			public Matrix4 ViewMatrix;
			public Matrix4 ProjectionMatrix;
			public Fbo ShadowMapFbo;
			public bool Using;
		}

		public const int MaxLightCount = 4;
		public const int ShadowMapSize = 4096;
		private static Fbo _multisampleFbo;
		private static int _width = 1;
		private static int _height = 1;

		private static List<LightCache> _lightCache = new List<LightCache>();
		private static Matrix4 _viewMatrixCache = Matrix4.Identity;
		private static Matrix4 _projectionMatrixCache = Matrix4.Identity;
		private static Scene _sceneCache;
		private static List<Scene> _modelsCache;
		private static bool _texturingCache = true;

		#endregion

		#region OpenGL Init / Clear

		public static void InitGL()
		{
			// vypis info o systeme OpenGL
			Debug.WriteLine("OpenGL vendor: " + GL.GetString(StringName.Vendor));
			Debug.WriteLine("OpenGL renderer: " + GL.GetString(StringName.Renderer));
			Debug.WriteLine("OpenGL version: " + GL.GetString(StringName.Version));
			Debug.WriteLine("OpenGL GLSL version: " + GL.GetString(StringName.ShadingLanguageVersion));

			int maxColorAttachments;
			GL.GetInteger(GetPName.MaxColorAttachments, out maxColorAttachments);
			Debug.WriteLine("Max FBO color attachments: " + maxColorAttachments);

			//float maximumAnisotropy;
			//GL.GetFloat(All.MaxTextureMaxAnisotropyExt, out maximumAnisotropy);
			//Debug.WriteLine("Max Anisotrphy filter samples: %i\n", int(maximumAnisotropy));

			int maxTexturesInVertexShader;
			GL.GetInteger(GetPName.MaxVertexTextureImageUnits, out maxTexturesInVertexShader);
			Debug.WriteLine("Max Vertex Shader Textures: " + maxTexturesInVertexShader);

			int maxMultisampleColor;
			GL.GetInteger(GetPName.MaxColorTextureSamples, out maxMultisampleColor);
			Debug.WriteLine("Max Color Texture Samples: " + maxMultisampleColor);

			int maxMultisampleDepth;
			GL.GetInteger(GetPName.MaxDepthTextureSamples, out maxMultisampleDepth);
			Debug.WriteLine("Max Depth Texture Samples: " + maxMultisampleDepth);

			int maxMultisampleInt;
			GL.GetInteger(GetPName.MaxIntegerSamples, out maxMultisampleInt);
			Debug.WriteLine("Max Integer Samples: " + maxMultisampleInt);

			// nastav cierne pozadie
			GL.ClearColor(1f, 1f, 1f, 1f);

			// nastav z-buffer testovanie
			//TODO: ?? GL.ClearDepth(1.0f);
			GL.Enable(EnableCap.DepthTest);
			GL.DepthFunc(DepthFunction.Lequal);

			// nastav stencil buffer
			GL.ClearStencil(0);

			GL.Enable(EnableCap.CullFace);

			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareMode, (int)TextureCompareMode.CompareRefToTexture);

			// nastav zmiesavaciu funkciu
			GL.Enable(EnableCap.Blend);
			GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

			// nastav vhodne mapovanie textur
			GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);
			GL.TexEnv(TextureEnvTarget.TextureEnv, TextureEnvParameter.TextureEnvMode, (int)All.Modulate);
			GL.ActiveTexture(TextureUnit.Texture0);

			GL.BindTexture(TextureTarget.Texture2DMultisample, 0);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
		}

		public static void InitShaders()
		{
			ShaderManager.AddShader("skybox", "shaders/skybox.vert", "shaders/skybox.frag");
			ShaderManager.AddShader("shadowMap", "shaders/shadow_map_pass.vert", "shaders/shadow_map_pass.frag");
			ShaderManager.AddShader("particleSystem", "shaders/particle.vert", "shaders/particle.geom", "shaders/particle.frag");
			ShaderManager.AddShader("phong", "shaders/phong_shadow_map.vert", "shaders/phong_shadow_map.frag");
			ShaderManager.AddShader("phongNormalMap", "shaders/phong_shadow_normal_map.vert", "shaders/phong_shadow_normal_map.frag");
			ShaderManager.AddShader("coordinates", "shaders/coordinates.vert", "shaders/coordinates.frag");
			ShaderManager.AddShader("noShading", "shaders/no_shading.vert", "shaders/no_shading.frag");
		}

		public static void ClearGL()
		{
			if (_multisampleFbo != null) _multisampleFbo.Clear();
			_lightCache.Clear();
		}

		#endregion

		private static void UpdateLightCache(int lightCount)
		{
			if (_lightCache.Count != lightCount) //TODO: just manage difference, no need to recreate whole cache
			{
				_lightCache.Clear();
				for (int i = 0; i < lightCount; ++i)
				{
					_lightCache.Add(new LightCache
					{
						ViewMatrix = Matrix4.Identity,
						ProjectionMatrix = Matrix4.Identity,
						ShadowMapFbo = Fbo.CreateFbo(ShadowMapSize, ShadowMapSize, 1, true),
						Using = false
					});
				}
			}
			else
			{
				for (int i = 0; i < _lightCache.Count; ++i)
					_lightCache[i].Using = false;
			}
		}

		public static void RenderScene(Scene scene, List<Scene> models, TextWriter tw, bool texturing, bool shadows, bool showHelp)
		{
			if (scene == null)
				return;

			_sceneCache = scene;
			_modelsCache = models;
			_texturingCache = texturing;

			UpdateLightCache(scene.Lights.Count);

			if (shadows)
			{
				GL.CullFace(CullFaceMode.Front);
				for (int i = 0; i < scene.Lights.Count; ++i)
				{
					if (scene.Lights[i].Type == LightType.Directional)
						RenderShadowPass(scene.Lights[i], _lightCache[i]);
				}
			}

			GL.CullFace(CullFaceMode.FrontAndBack);
			RenderNormalPass(scene, showHelp);

			if (showHelp && tw != null)
			{
				tw.Render();
				RenderShadowMap();
			}

			RenderLastPass();
		}

		#region Render passes

		public static void RenderShadowPass(LightNode light, LightCache lightCache)
		{
			// SETUP FBO
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, lightCache.ShadowMapFbo.Id);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			GL.Viewport(0, 0, ShadowMapSize, ShadowMapSize);

			// SET SHADER PROGRAM
			int shaderId = ShaderManager.GetShader("shadowMap");
			GL.UseProgram(shaderId);

			// SET PROJECTION MATRIX
			lightCache.ProjectionMatrix = Mathf.GetCameraProjectionBasedOnSceneAabb(light, new Vector3(-100, -100, -100), new Vector3(100, 100, 100), light.Type == LightType.Directional);
			Shader.SetUniformMatrix4(shaderId, "projectionMatrix", false, lightCache.ProjectionMatrix);

			// SET VIEW MATRIX
			lightCache.ViewMatrix = light.ModelMatrix.Inverted();
			Shader.SetUniformMatrix4(shaderId, "viewMatrix", false, lightCache.ViewMatrix);

			// RENDER
			for (int i = 0; i < _sceneCache.Nodes.Count; ++i)
				RenderNodeShadow(_sceneCache.Nodes[i], _sceneCache.Materials, Matrix4.Identity, shaderId);

			for (int j = 0; j < _modelsCache.Count; ++j)
			{
				for (int i = 0; i < _modelsCache[j].Nodes.Count; ++i)
				{
					RenderNodeShadow(_modelsCache[j].Nodes[i], _modelsCache[j].Materials, Matrix4.Identity, shaderId);
				}
			}
				

			// CLEANUP
			GL.UseProgram(0);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

			lightCache.Using = true;
		}

		public static void RenderNormalPass(Scene scene, bool showHelp)
		{
			// SETUP FBO
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, _multisampleFbo.Id);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			SetViewport();

			// SET PROJECTION & VIEW MATRIX CACHE
			float ratio = _width / (float)_height;
			_projectionMatrixCache = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver2, ratio, 0.1f, 1000f);
			_viewMatrixCache = scene.MainCamera.ViewMatrix;
			
			// RENDER WORLD COORDINATES
			if (showHelp)
				RenderCoords(Matrix4.Identity);

			// SET SHADOW MAPS
			var shadowMapTexUnit = TextureUnit.Texture7;
			for (int i = 0; i < _lightCache.Count; ++i)
			{
				GL.ActiveTexture(shadowMapTexUnit);
				GL.BindTexture(TextureTarget.Texture2D, _lightCache[i].ShadowMapFbo.DepthTexture);
				shadowMapTexUnit = shadowMapTexUnit.Next();
			}
			for (int i = _sceneCache.Lights.Count; i < MaxLightCount; ++i)
			{
				GL.ActiveTexture(shadowMapTexUnit);
				GL.BindTexture(TextureTarget.Texture2D, TextureManager.GetTexture("Default"));
				shadowMapTexUnit = shadowMapTexUnit.Next();
			}

			// RENDER SKYBOX
			if (scene.Skybox != null)
			{
				GL.CullFace(CullFaceMode.Front);
				RenderSkybox(scene.Skybox, scene.Materials); //TODO: je stred skyboxu stale v bode kamery ??  nope
				GL.CullFace(CullFaceMode.Back);
			}

			// RENDER LIGHTS
			for (int i = 0; i < _sceneCache.Lights.Count; ++i)
				RenderNode(scene.Lights[i], scene.Materials, Matrix4.Identity);

			// RENDER
			for (int i = 0; i < _sceneCache.Nodes.Count; ++i)
				RenderNode(_sceneCache.Nodes[i], _sceneCache.Materials, Matrix4.Identity);

			for (int j = 0; j < _modelsCache.Count; ++j)
			{
				for (int i = 0; i < _modelsCache[j].Nodes.Count; ++i)
				{
					RenderNode(_modelsCache[j].Nodes[i], _modelsCache[j].Materials, Matrix4.Identity);
				}
			}

			foreach (Emitter node in scene.ParticleSystems)
				RenderParticleSystem(node);

			//GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
		}

		public static void RenderLastPass()
		{
			GL.ActiveTexture(TextureUnit.Texture0);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

			GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, 0);				// Make sure no FBO is set as the draw framebuffer
			GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, _multisampleFbo.Id); // Make sure your multisampled FBO is the read framebuffer
			GL.DrawBuffer(DrawBufferMode.Back);										// Set the back buffer as the draw buffer
			GL.BlitFramebuffer(0, 0, _width, _height, 0, 0, _width, _height, ClearBufferMask.ColorBufferBit, BlitFramebufferFilter.Nearest);
			GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, 0);
			GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, 0);
		}

		#endregion

		#region Private Setters

		private static void SetViewport()
		{
			GL.Viewport(0, 0, _width, _height); // Use all of the glControl painting area
		}

		#endregion

		#region Material Setters

		private static void SetActualMaterial(Material material, out int shaderId)
		{
			shaderId = ShaderManager.GetShader(material.Shader);
			if (shaderId == -1)
				shaderId = (material.NormalTextureGLId > 0) ? ShaderManager.GetShader("phongNormalMap") : ShaderManager.GetShader("phong");
			GL.UseProgram(shaderId);

			// zakladne vlastnosti materialu
			Shader.SetUniform4(shaderId, "material.ambient", material.ColorAmbient);
			Shader.SetUniform4(shaderId, "material.diffuse", material.ColorDiffuse);
			Shader.SetUniform4(shaderId, "material.specular", material.ColorSpecular);
			Shader.SetUniform4(shaderId, "material.emission", material.ColorEmission);
			Shader.SetUniform1(shaderId, "material.shininess", material.Shininess);

			// nastav aktualnu texturu
			GL.ActiveTexture(TextureUnit.Texture0);
			if (material.DiffuseTextureGLId > 0)
				GL.BindTexture(TextureTarget.Texture2D, material.DiffuseTextureGLId);
			else
				GL.BindTexture(TextureTarget.Texture2D, TextureManager.GetTexture("Default"));
			Shader.SetUniform1(shaderId, "diffuse_map", 0);

			GL.ActiveTexture(TextureUnit.Texture1);
			if (material.NormalTextureGLId > 0)
				GL.BindTexture(TextureTarget.Texture2D, material.NormalTextureGLId);
			else
				GL.BindTexture(TextureTarget.Texture2D, TextureManager.GetTexture("Default"));
			Shader.SetUniform1(shaderId, "normal_map", 1);
		}

		private static void SetDefaultMaterial(out int shaderId)
		{
			shaderId = ShaderManager.GetShader("phong");
			GL.UseProgram(shaderId);

			// zakladne vlastnosti materialu
			Shader.SetUniform4(shaderId, "material.ambient", new Color4(.2f, .2f, .2f, 1f));
			Shader.SetUniform4(shaderId, "material.diffuse", new Color4(.8f, .8f, .8f, 1f));
			Shader.SetUniform4(shaderId, "material.specular", new Color4(1f, 1f, 1f, 1f));
			Shader.SetUniform4(shaderId, "material.emission", new Color4(0f, 0f, 0f, 1f));
			Shader.SetUniform1(shaderId, "material.shininess", 16);
		}

		#endregion

		#region Render functions

		public static void RenderNode(Node node, Dictionary<string, Material> materials, Matrix4 modelMatrix)
		{
			//if (node.Mesh != null)
				modelMatrix *= node.ModelMatrix;
			//else
				//modelMatrix = node.ModelMatrix;//TODO: *=

			RenderMesh(node.Mesh, materials, modelMatrix);

			foreach (Node childrenNode in node.ChildrenNodes)
				RenderNode(childrenNode, materials, modelMatrix);
		}

		public static void RenderNodeShadow(Node node, Dictionary<string, Material> materials, Matrix4 modelMatrix, int shaderId)
		{
			//if (node.Mesh != null)
				modelMatrix *= node.ModelMatrix;
			//else
				//modelMatrix = node.ModelMatrix;//TODO: *=

			Shader.SetUniformMatrix4(shaderId, "modelMatrix", false, modelMatrix);

			RenderMeshShadow(node.Mesh, shaderId);

			foreach (Node childrenNode in node.ChildrenNodes)
				RenderNodeShadow(childrenNode, materials, modelMatrix, shaderId);
		}


		public static void RenderMesh(Mesh mesh, Dictionary<string, Material> materials, Matrix4 modelMatrix)
		{
			if (mesh == null)
				return;

			// SET MATERIAL & RETURN SHADER //TODO: material.GetShader() not this way (out int ...)
			int shaderId;
			Material mat;
			if (materials.TryGetValue(mesh.MaterialName, out mat))
				SetActualMaterial(mat, out shaderId);
			else
			{
				if (materials.TryGetValue("Default", out mat))
					SetActualMaterial(mat, out shaderId);
				else
					SetDefaultMaterial(out shaderId);
			}

			// SET MVP
			Shader.SetUniformMatrix4(shaderId, "projectionMatrix", false, _projectionMatrixCache);
			Shader.SetUniformMatrix4(shaderId, "viewMatrix", false, _viewMatrixCache);
			Shader.SetUniformMatrix4(shaderId, "modelMatrix", false, modelMatrix);

			Shader.SetUniform1(shaderId, "texturing_enabled", (_texturingCache) ? 1 : 0);

			// SET LIGHTS //TODO: better place
			for (int i = 0; i < _sceneCache.Lights.Count; ++i)
			{
				_sceneCache.Lights[i].SetGL(i, shaderId);
				Shader.SetUniformMatrix4(shaderId, "lightSource[" + i + "].viewMatrix", false, _lightCache[i].ViewMatrix);
				Shader.SetUniformMatrix4(shaderId, "lightSource[" + i + "].projectionMatrix", false, _lightCache[i].ProjectionMatrix);
				Shader.SetUniform1(shaderId, "lightSource[" + i + "].hasShadowMap", (_lightCache[i].Using) ? 1 : 0);
				Shader.SetUniform1(shaderId, "shadowMaps[" + i + "]", 7 + i);
			}
			for (int i = _sceneCache.Lights.Count; i < MaxLightCount; ++i)
			{
				LightNode.SetDefaultGL(i, shaderId);
				Shader.SetUniform1(shaderId, "shadowMaps[" + i + "]", 7 + i);
			}

			// 1rst attribute buffer : vertices
			int vertexAttribIndex = GL.GetAttribLocation(shaderId, "a_vertex");
			GL.EnableVertexAttribArray(vertexAttribIndex);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboVerticesId);
			GL.VertexAttribPointer(vertexAttribIndex, 3, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);
 
			// 2nd attribute buffer : UVs
			int texCoordAttribIndex = GL.GetAttribLocation(shaderId, "a_texcoord");
			GL.EnableVertexAttribArray(texCoordAttribIndex);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboTexCoordsId);
			GL.VertexAttribPointer(texCoordAttribIndex, 2, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);
 
			// 3rd attribute buffer : normals
			int normalAttribIndex = GL.GetAttribLocation(shaderId, "a_normal");
			GL.EnableVertexAttribArray(normalAttribIndex);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboNormalsId);
			GL.VertexAttribPointer(normalAttribIndex, 3, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);
 
			// 4th attribute buffer : tangents
			int tangentAttribIndex = GL.GetAttribLocation(shaderId, "a_tangent");
			GL.EnableVertexAttribArray(tangentAttribIndex);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboTangentsId);
			GL.VertexAttribPointer(tangentAttribIndex, 3, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);
 
			// 5th attribute buffer : bitangents
			int bitangentAttribIndex = GL.GetAttribLocation(shaderId, "a_bitangent");
			GL.EnableVertexAttribArray(bitangentAttribIndex);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboBitangentsId);
			GL.VertexAttribPointer(bitangentAttribIndex, 3, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);

			// vykreslenie geometricky primitiv s pomocou VBO
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, mesh.VboIndicesId);
			var mode = PrimitiveType.Points;
			switch (mesh.NumFaceIndices)
			{
				case 2:
					mode = PrimitiveType.Lines;
					break;
				case 3:
					mode = PrimitiveType.Triangles;
					break;
				case 4:
					mode = PrimitiveType.Quads;
					break;
			}
			GL.DrawElements(mode, mesh.NumFaceIndices * mesh.NumFaces, DrawElementsType.UnsignedInt, 0);

			// deaktivacia bufferov
			GL.DisableVertexAttribArray(vertexAttribIndex);
			GL.DisableVertexAttribArray(texCoordAttribIndex);
			GL.DisableVertexAttribArray(normalAttribIndex);
			GL.DisableVertexAttribArray(tangentAttribIndex);
			GL.DisableVertexAttribArray(bitangentAttribIndex);
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

			GL.UseProgram(0);
		}

		public static void RenderMeshShadow(Mesh mesh, int shaderId)
		{
			if (mesh == null)
				return;

			// 1rst attribute buffer : vertices
			int vertexAttribIndex = GL.GetAttribLocation(shaderId, "a_vertex");
			GL.EnableVertexAttribArray(vertexAttribIndex);
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.VboVerticesId);
			GL.VertexAttribPointer(vertexAttribIndex, 3, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);

			// vykreslenie geometricky primitiv s pomocou VBO
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, mesh.VboIndicesId);
			var mode = PrimitiveType.Points;
			switch (mesh.NumFaceIndices)
			{
				case 2:
					mode = PrimitiveType.Lines;
					break;
				case 3:
					mode = PrimitiveType.Triangles;
					break;
				case 4:
					mode = PrimitiveType.Quads;
					break;
			}
			GL.DrawElements(mode, mesh.NumFaceIndices * mesh.NumFaces, DrawElementsType.UnsignedInt, 0);

			// deaktivacia bufferov
			GL.DisableVertexAttribArray(vertexAttribIndex);
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
		}

		public static void RenderSkybox(Mesh mesh, Dictionary<string, Material> materials)
		{
			int shaderId = ShaderManager.GetShader("skybox");
			GL.UseProgram(shaderId);

			Material mat;
			if (materials.TryGetValue(mesh.MaterialName, out mat))
			{
				GL.ActiveTexture(TextureUnit.Texture0);
				if (mat.DiffuseTextureGLId > 0)
					GL.BindTexture(TextureTarget.Texture2D, mat.DiffuseTextureGLId);
				else
					GL.BindTexture(TextureTarget.Texture2D, TextureManager.GetTexture("Default"));
				Shader.SetUniform1(shaderId, "cube_map", 0);
			}
			else
			{
				if (materials.TryGetValue("Default", out mat))
					SetActualMaterial(mat, out shaderId);
				else
					SetDefaultMaterial(out shaderId);
			}

			Shader.SetUniformMatrix4(shaderId, "projectionMatrix", false, _projectionMatrixCache);
			Shader.SetUniformMatrix4(shaderId, "viewMatrix", false, _viewMatrixCache);
			Shader.SetUniformMatrix4(shaderId, "modelMatrix", false, Matrix4.Identity);

			// to ad model transform look out
			//http://ogldev.atspace.co.uk/www/tutorial25/tutorial25.html
			RenderMeshShadow(mesh, shaderId);

			GL.UseProgram(0);
		}

		public static void RenderParticleSystem(Emitter emitter)
		{
			int shaderId = ShaderManager.GetShader("particleSystem");
			GL.UseProgram(shaderId);

			Shader.SetUniformMatrix4(shaderId, "projectionMatrix", false, _projectionMatrixCache);
			Shader.SetUniformMatrix4(shaderId, "viewMatrix", false, _viewMatrixCache);
			Shader.SetUniformMatrix4(shaderId, "modelMatrix", false, emitter.ModelMatrix);

			GL.Enable(EnableCap.Blend);
			GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

			GL.Enable(EnableCap.Texture2D);
			GL.ActiveTexture(TextureUnit.Texture0);
			GL.BindTexture(TextureTarget.Texture2D, emitter.DiffuseTextureGLId);
			Shader.SetUniform1(shaderId, "diffuseTexture", 0);

			GL.ActiveTexture(TextureUnit.Texture1);
			GL.BindTexture(TextureTarget.Texture2D, emitter.DiffuseTexture2GLId);
			Shader.SetUniform1(shaderId, "diffuseTexture2", 1);

			GL.BindBuffer(BufferTarget.ArrayBuffer, emitter.ParticlesPositionBuffer);

			int posAttrib = GL.GetAttribLocation(shaderId, "a_position");
			GL.EnableVertexAttribArray(posAttrib);
			GL.VertexAttribPointer(posAttrib, 3, VertexAttribPointerType.Float, false, 5 * sizeof(float), 0);

			int sizeAttrib = GL.GetAttribLocation(shaderId, "a_size");
			GL.EnableVertexAttribArray(sizeAttrib);
			GL.VertexAttribPointer(sizeAttrib, 1, VertexAttribPointerType.Float, false, 5 * sizeof(float), 3 * sizeof(float));

			int lifeAttrib = GL.GetAttribLocation(shaderId, "a_life");
			GL.EnableVertexAttribArray(lifeAttrib);
			GL.VertexAttribPointer(lifeAttrib, 1, VertexAttribPointerType.Float, false, 5 * sizeof(float), 4 * sizeof(float));

			GL.DrawArrays(PrimitiveType.Points, 0, emitter.ParticleCount);

			GL.Disable(EnableCap.Texture2D);
			GL.DisableVertexAttribArray(lifeAttrib);
			GL.DisableVertexAttribArray(sizeAttrib);
			GL.DisableVertexAttribArray(posAttrib);
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
			GL.UseProgram(0);
		}

		//TODO: get rid of fixed pipeline
		private static void RenderShadowMap()
		{
			GL.MatrixMode(MatrixMode.Projection);
			GL.PushMatrix();
			GL.LoadIdentity();
			GL.Ortho(0, _width, 0, _height, -1, 1);
			GL.MatrixMode(MatrixMode.Modelview);
			GL.LoadIdentity();
			GL.ActiveTexture(TextureUnit.Texture0);
			GL.Enable(EnableCap.Texture2D);
			GL.BindTexture(TextureTarget.Texture2D, _lightCache[0].ShadowMapFbo.DepthTexture);

			GL.Begin(PrimitiveType.Quads);
			GL.TexCoord2(0, 0); GL.Vertex3(0, 0, 0);
			GL.TexCoord2(1, 0); GL.Vertex3(0.3f * _height, 0, 0);
			GL.TexCoord2(1, 1); GL.Vertex3(0.3f * _height, 0.3f * _height, 0);
			GL.TexCoord2(0, 1); GL.Vertex3(0, 0.3f * _height, 0);
			GL.End();

			GL.Disable(EnableCap.Texture2D);

			GL.MatrixMode(MatrixMode.Projection);
			GL.PopMatrix();
		}

		private static void RenderCoords(Matrix4 model)
		{
			int shaderId = ShaderManager.GetShader("coordinates");
			GL.UseProgram(shaderId);

			Shader.SetUniformMatrix4(shaderId, "projectionMatrix", false, _projectionMatrixCache);
			Shader.SetUniformMatrix4(shaderId, "viewMatrix", false, _viewMatrixCache);
			Shader.SetUniformMatrix4(shaderId, "modelMatrix", false, model);

			GL.LineWidth(1);
			GL.Begin(PrimitiveType.Lines);
				GL.Color3(1.0f, 0.0f, 0.0f); GL.Vertex3(0.0f, 0.0f, 0.0f); GL.Vertex3(100.0f, 0.0f, 0.0f);
				GL.Color3(0.0f, 1.0f, 0.0f); GL.Vertex3(0.0f, 0.0f, 0.0f); GL.Vertex3(0.0f, 100.0f, 0.0f);
				GL.Color3(0.0f, 0.0f, 1.0f); GL.Vertex3(0.0f, 0.0f, 0.0f); GL.Vertex3(0.0f, 0.0f, 100.0f);
			GL.End();

			GL.UseProgram(0);
		}

		#endregion

		#region Events

		public static void Resize(int width, int height)
		{
			_width = width;
			_height = height;
			if (_multisampleFbo != null) _multisampleFbo.Clear();
			_multisampleFbo = Fbo.CreateFbo(_width, _height, 1, true);
			SetViewport();
		}

		#endregion
	};
}
