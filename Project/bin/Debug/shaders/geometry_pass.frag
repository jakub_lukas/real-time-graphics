uniform sampler2D displacement_map;
varying vec3 N_eye;
varying float linear_depth_eye;

uniform sampler2D diffuse_map;
uniform int texturing_enabled;

vec4 pack_depth(float depth)
{
    const vec4 bit_shift = vec4(256.0*256.0*256.0, 256.0*256.0, 256.0, 1.0);
    const vec4 bit_mask  = vec4(0.0, 1.0/256.0, 1.0/256.0, 1.0/256.0);
    vec4 res = fract(depth * bit_shift);
    res -= res.xxyz * bit_mask;
    return res;
}

void main(void)
{
   vec4 diffuse_material = gl_FrontMaterial.diffuse;
   if (texturing_enabled > 0) {
       diffuse_material = texture2D(diffuse_map, gl_TexCoord[0].st);
   } else {
	   float depth = texture2D(displacement_map, gl_TexCoord[0].st).r;
	   vec4 colorWhite = vec4(1, 1, 1, 1);
	   vec4 colorBrown = vec4(0.5, 0.25, 0, 1);
	   vec4 colorGreen = vec4(0, 1, 0, 1);
	   vec4 color;
	   if (depth >= 0.5){
		   float tmp = (depth - 0.5) * 2;
		   color = (1 - tmp) * colorBrown + tmp * colorWhite;
	   } else {
	       float tmp = depth * 2;
		   color = (1 - tmp) * colorGreen + tmp * colorBrown;
	   }
       diffuse_material = color;
   }

   gl_FragData[0] = diffuse_material;
   gl_FragData[1] = vec4(N_eye.xyz, linear_depth_eye);
   gl_FragData[2] = pack_depth(linear_depth_eye);
}

