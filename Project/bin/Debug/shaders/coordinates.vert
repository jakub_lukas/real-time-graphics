#version 150 compatibility

#ifdef GL_ES
	precision mediump float;
	precision mediump int;
	precision lowp sampler2D;
	precision lowp samplerCube;
#endif

//////////////////////////////////////////////////////////////

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

varying vec4 color;

void main(void)
{
	color = gl_Color;
	gl_Position = projectionMatrix * viewMatrix * modelMatrix * gl_Vertex;
}

