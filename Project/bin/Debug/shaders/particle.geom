layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

uniform mat4 projectionMatrix;

in float vSize[];
in float vLife[];

out vec2 texCoords;
out float life;

void main(void)
{
	float halfSize = vSize[0] / 2f * ((1 - vLife[0]) / 2f + 0.5);

	gl_Position = projectionMatrix * (gl_in[0].gl_Position + vec4(-halfSize, -halfSize, 0.0, 0.0));
	texCoords = vec2(0, 0);
	life = vLife[0];
	EmitVertex();

	gl_Position = projectionMatrix * (gl_in[0].gl_Position + vec4(halfSize, -halfSize, 0.0, 0.0));
	texCoords = vec2(1, 0);
	life = vLife[0];
	EmitVertex();

	gl_Position = projectionMatrix * (gl_in[0].gl_Position + vec4(-halfSize, halfSize, 0.0, 0.0));
	texCoords = vec2(0, 1);
	life = vLife[0];
	EmitVertex();

	gl_Position = projectionMatrix * (gl_in[0].gl_Position + vec4(halfSize, halfSize, 0.0, 0.0));
	texCoords = vec2(1, 1);
	life = vLife[0];
	EmitVertex();

	EndPrimitive();
}