uniform sampler2D diffuseTexture;
uniform sampler2D diffuseTexture2;

in vec2 texCoords;
in float life;

void main(void)
{
	vec4 texColor = texture(diffuseTexture, texCoords);
	vec4 texColor2 = texture(diffuseTexture2, texCoords);
	if (texColor.a < 0.01f && texColor2.a < 0.01f)
		discard;

	gl_FragColor = life * texColor + (1 - life) * texColor2;
}