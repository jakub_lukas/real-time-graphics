#version 150

#ifdef GL_ES
	precision mediump float;
	precision mediump int;
	precision lowp sampler2D;
	precision lowp samplerCube;
#endif

void main(void)
{
   gl_FragColor = vec4(0, 0, 0, 0);
}
