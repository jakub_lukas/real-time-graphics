#version 150

#ifdef GL_ES
	precision mediump float;
	precision mediump int;
	precision lowp sampler2D;
	precision lowp samplerCube;
#endif

//////////////////////////////////////////////////////////////

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

attribute vec3 a_vertex;

void main(void)
{
   gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(a_vertex, 1.0);
}
