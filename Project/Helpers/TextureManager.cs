﻿using System.Collections.Generic;
using System.Linq;

namespace Project.Helpers
{
	class TextureManager
	{
		private static readonly Dictionary<string, int> Textures = new Dictionary<string, int>(); 

		public static int AddTexture(string name, string dirName, string fileName)
		{
			char last = dirName.Last();
			if (last != '/' && last != '\\')
				dirName += "/";
			string path = Texture.GetPath(dirName, fileName);
			int glIndex = Texture.Load(path);
			Textures[name] = glIndex;

			return glIndex;
		}

		public static int AddTexture(string name, string path)
		{
			int glIndex = Texture.Load(path);
			if (glIndex == 0)
				return 0;

			Textures[name] = glIndex;
			return glIndex;
		}

		public static void RemoveTexture(string name)
		{
			if (Textures.ContainsKey(name))
				Textures.Remove(name);
		}

		public static int GetTexture(string name)
		{
			int result;
			if (Textures.TryGetValue(name, out result))
				return result;

			return 0;
		}
	}
}
