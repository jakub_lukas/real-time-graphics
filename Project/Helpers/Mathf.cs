﻿using System.Collections.Generic;
using OpenTK;
using Project.Scene.Helpers;

namespace Project.Helpers
{
	static class Mathf
	{
		public const float BasicNearPlane = 0.1f;

		public static Matrix4 GetOrthoMatrix(float left, float right, float bottom, float top, float zNear, float zFar)
		{
			return new Matrix4(
				2f / (right - left), 0f, 0f, 0f,
				0f, 2f / (top - bottom), 0, 0f,
				0f, 0f, -2f / (zFar - zNear), 0f,
				-(right + left) / (right - left), -(top + bottom) / (top - bottom), -(zFar + zNear) / (zFar - zNear), 1f
			);
		}

		public static Matrix4 GetFrustumMatrix(float left, float right, float bottom, float top, float zNear, float zFar)
		{
			return new Matrix4(
				2f * zNear / (right - left), 0f, 0f, 0f,
				0f, (2f * zNear) / (top - bottom), 0f, 0f,
				(right + left) / (right - left), (top + bottom) / (top - bottom), -(zFar + zNear) / (zFar - zNear), -1f,
				0f, 0f, -2f * zFar * zNear / (zFar - zNear), 0f
			);
		}

		public static Vector3 TransformPointToProjectionPlane(Movable cameraPose, Vector3 point, bool ortho)
		{
			// vyrob vektory v svetovych suradniciach ktore urcuje suradnicovy system kamery
			Vector3 dir = -cameraPose.AxisZ;//Direction;

			// vyrob rovnicu priemetne pre pohlad kamery
			// kedze dir je kolmy na priemetnu, prve tri parametre priemetne su dir_x, dir_y, dir_z
			// rovnica priemetne je: dir.x * x + dir.y * y + dir.z * z + d = 0
			var middle = new Vector3(cameraPose.Position.X + BasicNearPlane * dir.X, cameraPose.Position.Y + BasicNearPlane * dir.Y, cameraPose.Position.Z + BasicNearPlane * dir.Z);
			float d = -(dir.X * middle.X + dir.Y * middle.Y + dir.Z * middle.Z);

			// pre kazdy vrchol AABB sceny, urob priemet do priemetne a vypocitaj lokalne suradnice priemetu
			Vector3 inter;
			float depth;
			if (ortho)
			{
				float t = -(d + Vector3.Dot(point, dir)) / Vector3.Dot(dir, dir);
				inter.X = point.X + t * dir.X;
				inter.Y = point.Y + t * dir.Y;
				inter.Z = point.Z + t * dir.Z;
				depth = -t;
			}
			else
			{
				var PMinusC = new Vector3(point.X - cameraPose.Position.X, point.Y - cameraPose.Position.Y, point.Z - cameraPose.Position.Z);
				float t = -(d + Vector3.Dot(dir, cameraPose.Position)) / Vector3.Dot(dir, PMinusC);
				// ak je t zaporne, bod P je az za kamerou, cize pri perspektive neviditelny, vtedy tento postup az tak nefunguje
				if (t < 0)
					t = 0;
				inter.X = cameraPose.Position.X + t * PMinusC.X;
				inter.Y = cameraPose.Position.Y + t * PMinusC.Y;
				inter.Z = cameraPose.Position.Z + t * PMinusC.Z;
				depth = Vector3.Dot(dir, PMinusC);
			}
			var localVector = new Vector3(inter.X - middle.X, inter.Y - middle.Y, inter.Z - middle.Z);
			return new Vector3(Vector3.Dot(cameraPose.AxisX, localVector), Vector3.Dot(cameraPose.AxisY, localVector), depth);
		}

		public static void ProjectSceneAabb(Movable cameraPose, Vector3 sceneMin, Vector3 sceneMax, bool ortho,
			out float left, out float right, out float bottom, out float top, out float near, out float far)
		{
			// zoznam obsahujuci vsetkych 8 vrcholov AABB sceny pretransformovanych do priestoru priemetne
			var transformedCorners = new List<Vector3>
			{
				TransformPointToProjectionPlane(cameraPose, new Vector3(sceneMin.X, sceneMin.Y, sceneMin.Z), ortho),
				TransformPointToProjectionPlane(cameraPose, new Vector3(sceneMax.X, sceneMin.Y, sceneMin.Z), ortho),
				TransformPointToProjectionPlane(cameraPose, new Vector3(sceneMin.X, sceneMax.Y, sceneMin.Z), ortho),
				TransformPointToProjectionPlane(cameraPose, new Vector3(sceneMin.X, sceneMin.Y, sceneMax.Z), ortho),
				TransformPointToProjectionPlane(cameraPose, new Vector3(sceneMax.X, sceneMax.Y, sceneMin.Z), ortho),
				TransformPointToProjectionPlane(cameraPose, new Vector3(sceneMin.X, sceneMax.Y, sceneMax.Z), ortho),
				TransformPointToProjectionPlane(cameraPose, new Vector3(sceneMax.X, sceneMin.Y, sceneMax.Z), ortho),
				TransformPointToProjectionPlane(cameraPose, new Vector3(sceneMax.X, sceneMax.Y, sceneMax.Z), ortho)
			};

			// najdi extremalne hodnoty
			left = bottom = near = float.MaxValue;
			right = top = far = float.MinValue;
			for (int i = 0; i < transformedCorners.Count; i++)
			{
				if (transformedCorners[i].X < left)
					left = transformedCorners[i].X;
				if (transformedCorners[i].X > right)
					right = transformedCorners[i].X;
				if (transformedCorners[i].Y < bottom)
					bottom = transformedCorners[i].Y;
				if (transformedCorners[i].Y > top)
					top = transformedCorners[i].Y;
				if (transformedCorners[i].Z < near)
					near = transformedCorners[i].Z;
				if (transformedCorners[i].Z > far)
					far = transformedCorners[i].Z;
			}

			if (!ortho)
			{
				left = left * near / BasicNearPlane;
				right = right * near / BasicNearPlane;
				bottom = bottom * near / BasicNearPlane;
				top = top * near / BasicNearPlane;
			}
		}

		public static Matrix4 GetCameraProjectionBasedOnSceneAabb(Movable cameraPose, Vector3 sceneMin, Vector3 sceneMax, bool ortho)
		{
			float left, right, bottom, top, near, far;

			ProjectSceneAabb(cameraPose, sceneMin, sceneMax, ortho, out left, out right, out bottom, out top, out near, out far);

			if (!ortho)
				return GetFrustumMatrix(left, right, bottom, top, near, far);

			return GetOrthoMatrix(left, right, bottom, top, near, far);
		}
	}
}
