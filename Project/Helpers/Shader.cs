﻿using System;
using System.Diagnostics;
using System.IO;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Project.Helpers
{
	static class Shader
	{
		public enum CachedUniforms
		{
			ModelMatrix,
			ViewMatrix,
			ProjectionMatrix
		}

		static public void Compile(int shader, string source)
		{
			GL.ShaderSource(shader, source);
			GL.CompileShader(shader);

			string info;
			GL.GetShaderInfoLog(shader, out info);
			info = info.Trim();
			Debug.WriteLine(info);

			int compileResult;
			GL.GetShader(shader, ShaderParameter.CompileStatus, out compileResult);
			if (compileResult != 1)
			{
				Debug.WriteLine("Compile Error!");
				Debug.WriteLine(source);

				Debug.WriteLine("Failed compiling shader");
				string log = GL.GetShaderInfoLog(shader);
				Debug.WriteLine(log);

				GL.DeleteShader(shader);
			}
		}

		// funkcia pre nacitanie textoveho suboru do pola znakov
		static public string ReadFile(string fileName)
		{
			var shaderReader = new StreamReader(fileName);
			return shaderReader.ReadToEnd();
		}

		// nacitanie zdrojoveho suboru shadera a ulozenie do OpenGL shadera
		// navratova hodnota je OpenGL id shadera
		static int Load(ShaderType shaderType, string fileName)
		{
			try
			{
				string shaderSource = ReadFile(fileName);
				if (shaderSource == null)
					return 0;

				int shader = GL.CreateShader(shaderType);
				Compile(shader, shaderSource);

				return shader;
			}
			catch (Exception e)
			{
				Debug.WriteLine("Shader not loaded: " + fileName);
				return 0;
			}
		}

		// nacitanie sahderov a ich ulozenie do OpenGL
		// navratova hodnota je OpenGL id shader programu
		public static int LoadProgram(string fileVertexName, string fileFragmentName)
		{
			int vertexShader = Load(ShaderType.VertexShader, fileVertexName);
			if (vertexShader == 0)
				return 0;
			int fragmentShader = Load(ShaderType.FragmentShader, fileFragmentName);
			if (fragmentShader == 0)
			{
				GL.DeleteShader(vertexShader);
				return 0;
			}

			// vytvorenie program objektu a pripojenie shaderov
			int programObj = GL.CreateProgram();
			GL.AttachShader(programObj, vertexShader);
			GL.AttachShader(programObj, fragmentShader);
			GL.LinkProgram(programObj);

			string info;
			GL.GetProgramInfoLog(programObj, out info);
			Debug.WriteLine(info);

			// zisti uspesnost zlinkovania shaderov
			int compileResult;
			GL.GetProgram(programObj, GetProgramParameterName.LinkStatus, out compileResult);
			if (compileResult != 1)
			{
				Debug.WriteLine("Failed linking shaders with error");
				string log = GL.GetProgramInfoLog(programObj);
				Debug.WriteLine(log);
				GL.DeleteProgram(programObj);
				GL.DeleteShader(fragmentShader);
				GL.DeleteShader(vertexShader);
			}
			return programObj;
		}

		// nacitanie sahderov a ich ulozenie do OpenGL
		// navratova hodnota je OpenGL id shader programu
		public static int LoadProgram(string fileVertexName, string fileGeometryName, string fileFragmentName)
		{
			int vertexShader = Load(ShaderType.VertexShader, fileVertexName);
			if (vertexShader == 0)
			{
				return 0;
			}

			int geometryShader = Load(ShaderType.GeometryShader, fileGeometryName);
			if (geometryShader == 0)
			{
				GL.DeleteShader(vertexShader);
				return 0;
			}

			int fragmentShader = Load(ShaderType.FragmentShader, fileFragmentName);
			if (fragmentShader == 0)
			{
				GL.DeleteShader(vertexShader);
				GL.DeleteShader(geometryShader);
				return 0;
			}

			// vytvorenie program objektu a pripojenie shaderov
			int programObj = GL.CreateProgram();
			GL.AttachShader(programObj, vertexShader);
			GL.AttachShader(programObj, geometryShader);
			GL.AttachShader(programObj, fragmentShader);
			GL.LinkProgram(programObj);

			string info;
			GL.GetProgramInfoLog(programObj, out info);
			Debug.WriteLine(info);

			// zisti uspesnost zlinkovania shaderov
			int compileResult;
			GL.GetProgram(programObj, GetProgramParameterName.LinkStatus, out compileResult);
			if (compileResult != 1)
			{
				Debug.WriteLine("Failed linking shaders with error");
				string log = GL.GetProgramInfoLog(programObj);
				Debug.WriteLine(log);
				GL.DeleteProgram(programObj);
			}

			//linked shaders or not, we should delete shader objects
			GL.DeleteShader(fragmentShader);
			GL.DeleteShader(geometryShader);
			GL.DeleteShader(vertexShader);
			return programObj;
		}

		public static void DeleteProgram(int programId)
		{
			GL.DeleteProgram(programId);
		}

		#region Uniform1

		public static bool SetUniform1(int program, string name, int value)
		{
			int location = GL.GetUniformLocation(program, name);

			if (location < 0)
				return false;

			GL.Uniform1(location, value);
			return true;
		}

		public static bool SetUniform1(int program, string name, float value)
		{
			int location = GL.GetUniformLocation(program, name);

			if (location < 0)
				return false;

			GL.Uniform1(location, value);
			return true;
		}

		public static bool SetUniform1(int program, string name, uint value)
		{
			int location = GL.GetUniformLocation(program, name);

			if (location < 0)
				return false;

			GL.Uniform1(location, value);
			return true;
		}

		#endregion

		#region Uniform3

		public static bool SetUniform3(int program, string name, float val1, float val2, float val3)
		{
			int location = GL.GetUniformLocation(program, name);

			if (location < 0)
				return false;

			GL.Uniform3(location, val1, val2, val3);
			return true;
		}

		public static bool SetUniform3(int program, string name, Vector3 value)
		{
			int location = GL.GetUniformLocation(program, name);

			if (location < 0)
				return false;

			GL.Uniform3(location, value);
			return true;
		}

		#endregion

		#region Uniform4

		public static bool SetUniform4(int program, string name, float val1, float val2, float val3, float val4)
		{
			int location = GL.GetUniformLocation(program, name);

			if (location < 0)
				return false;

			GL.Uniform4(location, val1, val2, val3, val4);
			return true;
		}

		public static bool SetUniform4(int program, string name, Vector4 value)
		{
			int location = GL.GetUniformLocation(program, name);

			if (location < 0)
				return false;

			GL.Uniform4(location, value);
			return true;
		}

		public static bool SetUniform4(int program, string name, Color4 value)
		{
			int location = GL.GetUniformLocation(program, name);

			if (location < 0)
				return false;

			GL.Uniform4(location, value);
			return true;
		}

		#endregion

		#region UniformMatrix3

		public static bool SetUniformMatrix3(int program, string name, bool transpose, float[] values)
		{
			int location = GL.GetUniformLocation(program, name);

			if (location < 0)
				return false;

			GL.UniformMatrix3(location, 1, transpose, values);
			return true;
		}

		public static bool SetUniformMatrix3(int program, string name, bool transpose, Matrix3 values)
		{
			int location = GL.GetUniformLocation(program, name);

			if (location < 0)
				return false;

			GL.UniformMatrix3(location, transpose, ref values);
			return true;
		}

		#endregion

		#region UniformMatrix4

		public static bool SetUniformMatrix4(int program, string name, bool transpose, float[] values)
		{
			int location = GL.GetUniformLocation(program, name);

			if (location < 0)
				return false;

			GL.UniformMatrix4(location, 1, transpose, values);
			return true;
		}

		public static bool SetUniformMatrix4(int program, string name, bool transpose, Matrix4 values)
		{
			int location = GL.GetUniformLocation(program, name);

			if (location < 0)
				return false;

			GL.UniformMatrix4(location, transpose, ref values);
			return true;
		}

		#endregion
	}
}
