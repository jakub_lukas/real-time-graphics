﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Project.Helpers
{
	class TextWriter
	{
		private readonly Font			_textFont = new Font(FontFamily.GenericSansSerif, 8);
		private readonly Bitmap			_textBitmap;
		private readonly List<PointF>	_positions;
		private readonly List<string>	_lines;
		private readonly List<Brush>	_colours;
		private readonly int			_textureId;
		private Size					_clientSize;

		public void Update(int ind, string newText)
		{
			if (ind >= _lines.Count) return;

			_lines[ind] = newText;
			UpdateText();
		}


		public TextWriter(Size clientSize, Size areaSize)
		{
			_positions = new List<PointF>();
			_lines = new List<string>();
			_colours = new List<Brush>();

			_textBitmap = new Bitmap(areaSize.Width, areaSize.Height);
			_clientSize = clientSize;
			_textureId = CreateTexture();
		}

		private int CreateTexture()
		{
			int textureId;
			GL.TexEnv(TextureEnvTarget.TextureEnv, TextureEnvParameter.TextureEnvMode, (float)TextureEnvMode.Replace);//Important, or wrong color on some computers
			Bitmap bitmap = _textBitmap;
			GL.GenTextures(1, out textureId);
			GL.BindTexture(TextureTarget.Texture2D, textureId);

			BitmapData data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
			GL.Finish();
			bitmap.UnlockBits(data);
			return textureId;
		}

		public void Dispose()
		{
			if (_textureId > 0)
				GL.DeleteTexture(_textureId);
		}

		public void Clear()
		{
			_lines.Clear();
			_positions.Clear();
			_colours.Clear();
		}

		public void AddLine(string s, PointF pos, Brush col)
		{
			_lines.Add(s);
			_positions.Add(pos);
			_colours.Add(col);
			UpdateText();
		}

		public void UpdateText()
		{
			if (_lines.Count > 0)
			{
				using (Graphics gfx = Graphics.FromImage(_textBitmap))
				{
					gfx.Clear(Color.Transparent);
					gfx.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
					for (int i = 0; i < _lines.Count; i++)
						gfx.DrawString(_lines[i], _textFont, _colours[i], _positions[i]);
				}

				BitmapData data = _textBitmap.LockBits(new Rectangle(0, 0, _textBitmap.Width, _textBitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
				GL.TexSubImage2D(TextureTarget.Texture2D, 0, 0, 0, _textBitmap.Width, _textBitmap.Height, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
				_textBitmap.UnlockBits(data);
			}
		}

		public void Render()
		{
			GL.MatrixMode(MatrixMode.Modelview);
			GL.LoadIdentity();

			GL.MatrixMode(MatrixMode.Projection);
			GL.PushMatrix();
			GL.Ortho(0, _clientSize.Width, 0, _clientSize.Height, -1, 1);

			GL.Enable(EnableCap.Blend);
			GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.DstColor);

			GL.ActiveTexture(TextureUnit.Texture0);
			GL.Enable(EnableCap.Texture2D);
			GL.BindTexture(TextureTarget.Texture2D, _textureId);

			GL.Begin(BeginMode.Quads);
				GL.TexCoord2(0, 1); GL.Normal3(0f, 0f, 1f); GL.Vertex3(0, _textBitmap.Height, 0);
				GL.TexCoord2(1, 1); GL.Normal3(0f, 0f, 1f); GL.Vertex3(_textBitmap.Width, _textBitmap.Height, 0);
				GL.TexCoord2(1, 0); GL.Normal3(0f, 0f, 1f); GL.Vertex3(_textBitmap.Width, _clientSize.Height, 0);
				GL.TexCoord2(0, 0); GL.Normal3(0f, 0f, 1f); GL.Vertex3(0, _clientSize.Height, 0);
			GL.End();

			GL.Disable(EnableCap.Blend);
			GL.Disable(EnableCap.Texture2D);

			GL.MatrixMode(MatrixMode.Projection);
			GL.PopMatrix();
		}
	}
}
