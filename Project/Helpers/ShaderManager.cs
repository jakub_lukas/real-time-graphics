﻿using System.Collections.Generic;

namespace Project.Helpers
{
	class ShaderManager
	{
		private static readonly Dictionary<string, int> Shaders = new Dictionary<string, int>(); 

		public static void AddShader(string name, string vertFileName, string fragFileName)
		{
			Shaders[name] = Shader.LoadProgram(vertFileName, fragFileName);
		}

		public static void AddShader(string name, string vertFileName, string geomFileName, string fragFileName)
		{
			Shaders[name] = Shader.LoadProgram(vertFileName, geomFileName, fragFileName);
		}

		public static void RemoveShader(string name)
		{
			if (Shaders.ContainsKey(name))
				Shaders.Remove(name);
		}

		public static int GetShader(string name)
		{
			int result;
			if (Shaders.TryGetValue(name, out result))
				return result;
			return -1;
		}
	}
}
