﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK.Graphics.OpenGL;

namespace Project.Helpers
{
	static class Texture
	{
		public const string Directory = "textures";

		// nacitanie textury z externeho suboru a jej ulozenie do OpenGL
		// navratova hodnota je OpenGL id textury 
		public static int Load(string texFile)
		{
			if (String.IsNullOrEmpty(texFile))
			{
				Debug.WriteLine("Can not open texture file " + texFile + " No texture loaded. ");
				return 0;
			}

			// generovanie OpenGL textur
			int tex = GL.GenTexture();
			GL.BindTexture(TextureTarget.Texture2D, tex);
			// nastavenie parametrov textury
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);

			try
			{
				var bmp = new Bitmap(texFile);
				BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

				GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bmpData.Width, bmpData.Height, 0,
					OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bmpData.Scan0);

				bmp.UnlockBits(bmpData);
			}
			catch (Exception)
			{
				Debug.WriteLine("Error ocured while loading texture " + texFile);
				return 0;
			}
            

			Debug.WriteLine("Texture " + texFile + " loaded successfully.");
			return tex;
		}

		public static void Unload(int textureGLId)
		{
			GL.DeleteTexture(textureGLId);
		}

		private static bool ValidateCube(string directory, string texPosXFile, string texNegXFile, string texPosYFile, string texNegYFile, string texPosZFile, string texNegZFile)
		{
			bool valid = true;
			string dir = directory + "/";
			if (String.IsNullOrEmpty(dir + texPosXFile))
			{
				Debug.WriteLine("Can not open texture file " + dir + texPosXFile + " No texture loaded. ");
				valid = false;
			}
			if (String.IsNullOrEmpty(dir + texNegXFile))
			{
				Debug.WriteLine("Can not open texture file " + dir + texNegXFile + " No texture loaded. ");
				valid = false;
			}
			if (String.IsNullOrEmpty(dir + texPosYFile))
			{
				Debug.WriteLine("Can not open texture file " + dir + texPosYFile + " No texture loaded. ");
				valid = false;
			}
			if (String.IsNullOrEmpty(dir + texNegYFile))
			{
				Debug.WriteLine("Can not open texture file " + dir + texNegYFile + " No texture loaded. ");
				valid = false;
			}
			if (String.IsNullOrEmpty(dir + texPosZFile))
			{
				Debug.WriteLine("Can not open texture file " + dir + texPosZFile + " No texture loaded. ");
				valid = false;
			}
			if (String.IsNullOrEmpty(dir + texNegZFile))
			{
				Debug.WriteLine("Can not open texture file " + dir + texNegZFile + " No texture loaded. ");
				valid = false;
			}

			return valid;
		}

		// nacitanie cube mapy z 6 textur a jej ulozenie do OpenGL
		// navratova hodnota je OpenGL id textury 
		public static int LoadCube(string directory, string texPosXFile, string texNegXFile, string texPosYFile, string texNegYFile, string texPosZFile, string texNegZFile)
		{
			if (!ValidateCube(directory, texPosXFile, texNegXFile, texPosYFile, texNegYFile, texPosZFile, texNegZFile))
				return 0;

			int cubeTex = GL.GenTexture();
			GL.BindTexture(TextureTarget.TextureCubeMap, cubeTex);

			Bitmap bmp;
			BitmapData bmpData;

			// nacitanie textury pre posx stenu zo suboru a do OpenGL
			bmp = new Bitmap(directory + "/" + texPosXFile);
			bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			GL.TexImage2D(TextureTarget.TextureCubeMapPositiveX, 0, PixelInternalFormat.Rgba8, bmpData.Width, bmpData.Height,
				0, OpenTK.Graphics.OpenGL.PixelFormat.Rgba, PixelType.UnsignedByte, bmpData.Scan0);
			bmp.UnlockBits(bmpData);

			// nacitanie textury pre negx stenu zo suboru a do OpenGL
			bmp = new Bitmap(directory + "/" + texNegXFile);
			bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			GL.TexImage2D(TextureTarget.TextureCubeMapNegativeX, 0, PixelInternalFormat.Rgba8, bmpData.Width, bmpData.Height,
				0, OpenTK.Graphics.OpenGL.PixelFormat.Rgba, PixelType.UnsignedByte, bmpData.Scan0);
			bmp.UnlockBits(bmpData);

			// nacitanie textury pre posy stenu zo suboru a do OpenGL
			bmp = new Bitmap(directory + "/" + texPosYFile);
			bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			GL.TexImage2D(TextureTarget.TextureCubeMapPositiveY, 0, PixelInternalFormat.Rgba8, bmpData.Width, bmpData.Height,
				0, OpenTK.Graphics.OpenGL.PixelFormat.Rgba, PixelType.UnsignedByte, bmpData.Scan0);
			bmp.UnlockBits(bmpData);

			// nacitanie textury pre negy stenu zo suboru a do OpenGL
			bmp = new Bitmap(directory + "/" + texNegYFile);
			bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			GL.TexImage2D(TextureTarget.TextureCubeMapNegativeY, 0, PixelInternalFormat.Rgba8, bmpData.Width, bmpData.Height,
				0, OpenTK.Graphics.OpenGL.PixelFormat.Rgba, PixelType.UnsignedByte, bmpData.Scan0);
			bmp.UnlockBits(bmpData);

			// nacitanie textury pre posz stenu zo suboru a do OpenGL
			bmp = new Bitmap(directory + "/" + texPosZFile);
			bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			GL.TexImage2D(TextureTarget.TextureCubeMapPositiveZ, 0, PixelInternalFormat.Rgba8, bmpData.Width, bmpData.Height,
				0, OpenTK.Graphics.OpenGL.PixelFormat.Rgba, PixelType.UnsignedByte, bmpData.Scan0);
			bmp.UnlockBits(bmpData);

			// nacitanie textury pre negz stenu zo suboru a do OpenGL
			bmp = new Bitmap(directory + "/" + texNegZFile);
			bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			GL.TexImage2D(TextureTarget.TextureCubeMapNegativeZ, 0, PixelInternalFormat.Rgba8, bmpData.Width, bmpData.Height,
				0, OpenTK.Graphics.OpenGL.PixelFormat.Rgba, PixelType.UnsignedByte, bmpData.Scan0);
			bmp.UnlockBits(bmpData);

			GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
			GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
			GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
			GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
			GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);
			GL.Enable(EnableCap.TextureCubeMapSeamless);

			Debug.WriteLine("Cube map loaded successfully.");

			return cubeTex;
		}

		public static string GetPath(string dirPath, string fileName)
		{
			if (fileName.StartsWith("./") || fileName.StartsWith("\\\\"))
				return dirPath + fileName.Substring(2);

			if (fileName.StartsWith("/") || fileName.StartsWith("\\"))
				return dirPath + fileName.Substring(1);

			return dirPath + fileName;
		}
	}
}
