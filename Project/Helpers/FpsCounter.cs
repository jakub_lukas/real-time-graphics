﻿using System.Diagnostics;

namespace Project.Helpers
{
    class FpsCounter
    {
        private readonly Stopwatch _stopWatch = new Stopwatch();

        public static float DeltaTime { get; private set; }
        public static int Fps { get; private set; }

        private double _timeAccumulator = 0; // accumulates passed miliseconds up to one second
        private int _fpsAccumulator = 0;
        private void Accumulate()
        {
            _fpsAccumulator++;
            _timeAccumulator += DeltaTime;

            if (!(_timeAccumulator > 1000))
                return;

            _timeAccumulator %= 1000;
            Fps = _fpsAccumulator;
            _fpsAccumulator = 0; // reset counter
        }

        public FpsCounter()
        {
            DeltaTime = 0;
        }

        public void Update()
        {
            _stopWatch.Stop();
            DeltaTime = (float)_stopWatch.Elapsed.TotalSeconds;
            _stopWatch.Reset();
            _stopWatch.Start();
            Accumulate();
        }

        public void Start()
        {
            _stopWatch.Start();
        }
    }
}

