﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Project.Inputs
{
	public class Input
	{
		public enum Axis { Horizontal, Vertical }

		public const float MouseMovementSpeed = .1f;
		public const float WheelSpeed = .01f;

		private static Input _instance;
		public static Input Instance
		{
			get
			{
				return _instance ?? (_instance = new Input());
			}
		}

		private static readonly List<MouseButtons> MouseDown = new List<MouseButtons>();
		private static Point _axisMovement;
		private static float _wheelMovement;

		private static readonly List<Keys> KeysDown = new List<Keys>();

		private Input() { }

		#region Mouse click

		public void SetMouseDown(MouseButtons button)
		{
			if (!MouseDown.Contains(button))
				MouseDown.Add(button);
		}

		public void SetMouseUp(MouseButtons button)
		{
			MouseDown.Remove(button);
		}

		public static bool GetMouse(MouseButtons button)
		{
			return MouseDown.Contains(button);
		}

		#endregion

		#region Mouse position

		public void SetMovement(int posX, int posY)
		{
			_axisMovement.X = posX;
			_axisMovement.Y = posY;
		}

		public static float GetAxis(Axis axis)
		{
			switch (axis)
			{
				case Axis.Horizontal:
					return _axisMovement.X * MouseMovementSpeed;

				case Axis.Vertical:
					return _axisMovement.Y * MouseMovementSpeed;
			}
			return 0f;
		}

		#endregion

		#region Mouse wheel

		public void SetMouseWheel(int wheelMovement)
		{
			_wheelMovement = wheelMovement * WheelSpeed;
		}

		public static float GetMouseWheel()
		{
			return _wheelMovement;
		}

		#endregion

		#region Keys

		public void SetKeyDown(Keys key)
		{
			if (!KeysDown.Contains(key))
				KeysDown.Add(key);
		}

		public void SetKeyUp(Keys key)
		{
			KeysDown.Remove(key);
		}

		public static bool GetKey(Keys key)
		{
			return KeysDown.Contains(key);
		}

		#endregion

		public void AfterUpdate()
		{
			_axisMovement.X = 0;
			_axisMovement.Y = 0;

			_wheelMovement = 0f;
		}
	}
}
