﻿using System;
using OpenTK;

namespace Project.Extensions
{
    static class QuaternionExt
    {
        public static Quaternion FromEuler(float eulerX, float eulerY, float eulerZ)
        {
            return new Quaternion(
                (float)(Math.PI * eulerY / 180),
                (float)(Math.PI * eulerZ / 180),
                (float)(Math.PI * eulerX / 180),
                1f
                );
        }

        public static Quaternion FromEuler(Vector3 eulers)
        {
            return FromEuler(eulers.X, eulers.Y, eulers.Z);
        }

        public static Quaternion DoubleArc(Quaternion q1, Quaternion q2)
        {
            // druhy sposob vypoctu zdvojenia
            float dot = q1.X*q2.X + q1.Y*q2.Y + q1.Z*q2.Z + q1.W*q2.W;
            return new Quaternion
            {
                X = 2 * dot * q2.X - q1.X,
                Y = 2 * dot * q2.Y - q1.Y,
                Z = 2 * dot * q2.Z - q1.Z,
                W = 2 * dot * q2.W - q1.W
            }.Normalized();
        }

        public static Quaternion BisectArc(Quaternion q1, Quaternion q2)
        {
            return new Quaternion
            {
                X = q1.X + q2.X,
                Y = q1.Y + q2.Y,
                Z = q1.Z + q2.Z,
                W = q1.W + q2.W
            }.Normalized();
        }
    }
}
