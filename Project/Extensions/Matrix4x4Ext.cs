﻿using Assimp;
using OpenTK;

namespace Project.Extensions
{
    static class Matrix4x4Ext
    {
        public static Matrix4x4 Transposed(this Matrix4x4 m)
        {
            var mat = new Matrix4x4(m);
            mat.Transpose();
            return mat;
        }

        public static Matrix4 ToMatrix4(this Matrix4x4 m)
        {
            return new Matrix4(
                m.A1, m.A2, m.A3, m.A4,
                m.B1, m.B2, m.B3, m.B4,
                m.C1, m.C2, m.C3, m.C4,
                m.D1, m.D2, m.D3, m.D4
                );
        }
    }
}
