﻿using System.Collections.Generic;
using Assimp;
using OpenTK;
using System.Linq;

namespace Project.Extensions
{
    static class Vector3DExt
    {
        public static Vector3 ToVector3(this Vector3D v)
        {
            return new Vector3(v.X, v.Y, v.Z);
        }

        public static float[] Vector3DArrayToGLArray(List<Vector3D> vectors)
        {
            float[] arr = new float[vectors.Count() * 3];
            int index = 0;
            foreach (Vector3D vertex in vectors)
            {
                arr[index] = vertex.X;
                arr[index + 1] = vertex.Y;
                arr[index + 2] = vertex.Z;
                index += 3;
            }
            return arr;
        }
    }
}
