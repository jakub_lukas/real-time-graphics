﻿using System;

namespace Project.Extensions
{
	public static class EnumExt
	{
		public static T Prev<T>(this T src) where T : struct
		{
			if (!typeof(T).IsEnum) throw new ArgumentException(String.Format("Argumnent {0} is not an Enum", typeof(T).FullName));

			T[] arr = (T[])Enum.GetValues(src.GetType());
			int j = Array.IndexOf(arr, src) - 1;
			return (j == -1) ? arr[arr.Length-1] : arr[j];
		}

		public static T Next<T>(this T src) where T : struct
		{
			if (!typeof(T).IsEnum) throw new ArgumentException(String.Format("Argumnent {0} is not an Enum", typeof(T).FullName));

			T[] arr = (T[])Enum.GetValues(src.GetType());
			int j = Array.IndexOf(arr, src) + 1;
			return (arr.Length == j) ? arr[0] : arr[j];
		}
	}
}
