﻿using Assimp;
using OpenTK.Graphics;

namespace Project.Extensions
{
    static class Color4DExt
    {
        public static Color4 ToColor4(this Color4D c)
        {
            return new Color4(c.R, c.G, c.B, c.A);
        }
    }
}
